#!/bin/bash

cd Bin
s=$(echo $1 | sed s/'.*\/Scripts\/\(.*\)'/'\1'/g)
echo Checking syntax for $s
./SyntaxChecker $s 2>&1 | awk '/ERROR/{print;}' | sed s/'\[.*\]\s'/''/g
