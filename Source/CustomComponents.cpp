#include "CustomComponents.h"
#include "MapGraphics.h"
#include "MapProperties.h"
#include "MapData.h"
#include "MapMouseToTile.h"
#include "MapPathfinder.h"
#include "UnitMovement.h"
#include "UnitStats.h"
#include "DmMain.h"
#include "DmCommander.h"
#include "DmStateInfo.h"
#include "GlobalEnums.h"
#include "PathfinderInfo.h"
#include "CameraMovement.h"
#include "GUIMain.h"
#include "MapDetail.h"
#include <Urho3D/Script/APITemplates.h>

void CustomComponents::RegisterObjects(Context* context) {
	MapProperties::RegisterObject(context);
	MapGraphics::RegisterObject(context);
	MapData::RegisterObject(context);
	MapMouseToTile::RegisterObject(context);
	MapPathfinder::RegisterObject(context);
	UnitMovement::RegisterObject(context);
	UnitStats::RegisterObject(context);
    GUIMain::RegisterObject(context);
    CameraMovement::RegisterObject(context);
    MapDetail::RegisterObject(context);
    DmMain::RegisterObject(context);
    DmStateInfo::RegisterObject(context);
    DmCommander::RegisterObject(context);
	//Add more custom components here...
}

void CustomComponents::BindScriptMethods(asIScriptEngine* engine) {
	GlobalEnumsBindScriptMethods(engine);
	MapProperties::BindScriptMethods(engine);
	MapGraphics::BindScriptMethods(engine);
	MapData::BindScriptMethods(engine);
	MapMouseToTile::BindScriptMethods(engine);
	PathfinderInfo::BindScriptMethods(engine);
	MapPathfinder::BindScriptMethods(engine);
	UnitMovement::BindScriptMethods(engine);
	UnitStats::BindScriptMethods(engine);
    CameraMovement::BindScriptMethods(engine);
    GUIMain::BindScriptMethods(engine);
    MapDetail::BindScriptMethods(engine);
    DmMain::BindScriptMethods(engine);
    DmStateInfo::BindScriptMethods(engine);
    DmCommander::BindScriptMethods(engine);
}
