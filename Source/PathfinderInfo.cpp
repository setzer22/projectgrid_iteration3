#include "PathfinderInfo.h"
#include <iostream>
using namespace std;

PathfinderInfo::PathfinderInfo(Vector<IntVector2>& MoveRange, Vector<IntVector2>& AttackRange, IntVector2 ini, vector<vector<float>>& DistMap, 
  MoveType UnitMoveType, Team UnitTeam) {
	this->MoveRange = new Vector<IntVector2>(MoveRange);
	this->AttackRange = new Vector<IntVector2>(AttackRange);
	this->DistMap = new vector<vector<float>>(DistMap);
	this->UnitMoveType = UnitMoveType;
	this->UnitTeam = UnitTeam;
	this->source = ini;
}

PathfinderInfo::~PathfinderInfo(){
	delete DistMap;
	delete AttackRange;
	delete MoveRange;
}

void PathfinderInfo::BindScriptMethods(asIScriptEngine *engine) { 
    int r;
    r = engine->RegisterObjectType("PathfinderInfo",0, asOBJ_REF); assert (r >= 0);
    r = engine->RegisterObjectBehaviour("PathfinderInfo", asBEHAVE_IMPLICIT_REF_CAST, "RefCounted@ f()", 
                asFUNCTION((RefCast<PathfinderInfo,RefCounted>)), asCALL_CDECL_OBJLAST); assert (r >= 0);
    r = engine->RegisterObjectBehaviour("RefCounted", asBEHAVE_IMPLICIT_REF_CAST, "PathfinderInfo@ f()", 
                asFUNCTION((RefCast<RefCounted,PathfinderInfo>)), asCALL_CDECL_OBJLAST); assert (r >= 0);
    r = engine->RegisterObjectBehaviour("PathfinderInfo", asBEHAVE_ADDREF, "void f()", asMETHOD(PathfinderInfo,AddRef), asCALL_THISCALL); assert (r >= 0);
    r = engine->RegisterObjectBehaviour("PathfinderInfo", asBEHAVE_RELEASE, "void f()", asMETHOD(PathfinderInfo,ReleaseRef), asCALL_THISCALL); assert (r >= 0);
    r = engine->RegisterObjectProperty("PathfinderInfo", "MoveType UnitMoveType", asOFFSET(PathfinderInfo,UnitMoveType)); assert (r >= 0);
    r = engine->RegisterObjectProperty("PathfinderInfo", "Team UnitTeam", asOFFSET(PathfinderInfo, UnitTeam)); assert(r >= 0);
    r = engine->RegisterObjectMethod("PathfinderInfo", "IntVector2 get_MoveRange(uint) const", asMETHOD(PathfinderInfo, get_MoveRange), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("PathfinderInfo", "uint moveRangeSize() const", asMETHOD(PathfinderInfo, moveRangeSize), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("PathfinderInfo", "IntVector2 get_AttackRange(uint) const", asMETHOD(PathfinderInfo, get_AttackRange), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("PathfinderInfo", "uint attackRangeSize() const", asMETHOD(PathfinderInfo, attackRangeSize), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("PathfinderInfo", "IntVector2 GetSource()", asMETHOD(PathfinderInfo, GetSource), asCALL_THISCALL);
}
