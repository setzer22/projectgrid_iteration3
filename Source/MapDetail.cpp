#include "MapDetail.h"
#include "Debug.h"

#include <Urho3D/Physics/CollisionShape.h>

MapDetail::MapDetail(Context* context) : LogicComponent(context) {

}

void MapDetail::RegisterObject(Context* context) {
    context->RegisterFactory<MapDetail>("Map");
}

void MapDetail::Start() {

}

void MapDetail::SetHighDetail(bool value) {
    if(value) {
        unitsNode->SetDeepEnabled(false);
        smUnitsNode->SetDeepEnabled(true);
        detailsNode->SetDeepEnabled(true);
        static_cast<CollisionShape*>(
            terrainNode->GetComponent<CollisionShape>())->SetTerrain(0);
    }
    else {
        unitsNode->SetDeepEnabled(true);
        smUnitsNode->SetDeepEnabled(false);
        detailsNode->SetDeepEnabled(false);
        static_cast<CollisionShape*>(
            terrainNode->GetComponent<CollisionShape>())->SetTerrain(3);
    }
}

void MapDetail::ApplyAttributes() {

    detailsNode = GetScene()->GetChild("Detail");
    TESTNULL(detailsNode, "Map", "Detail", "Details node not found on scene");

    unitsNode = GetScene()->GetChild("Units");
    TESTNULL(detailsNode, "Map", "Detail", "Units node not found on scene");

    smUnitsNode = GetScene()->GetChild("SmUnits");
    TESTNULL(detailsNode, "Map", "Detail", "SmUnits node not found on scene");

    terrainNode = GetScene()->GetChild("Terrain");
    TESTNULL(detailsNode, "Map", "Detail", "Terrain node not found on scene");

}

void MapDetail::BindScriptMethods(asIScriptEngine *engine) {
    int r;
    r = engine->RegisterObjectType("MapDetail",0, asOBJ_REF); assert (r>=0);
    r = engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "MapDetail@ f()",
        asFUNCTION((RefCast<Component,MapDetail>)), asCALL_CDECL_OBJLAST); assert (r>=0);
    r = engine->RegisterObjectBehaviour("MapDetail", asBEHAVE_ADDREF, "void f()", asMETHOD(MapDetail,AddRef), asCALL_THISCALL); assert (r>=0);
    r = engine->RegisterObjectBehaviour("MapDetail", asBEHAVE_RELEASE, "void f()", asMETHOD(MapDetail,ReleaseRef), asCALL_THISCALL); assert (r>=0);
    r = engine->RegisterObjectMethod("MapDetail", "void SetHighDetail(bool value)", asMETHOD(MapDetail, SetHighDetail), asCALL_THISCALL); assert (r>=0);
}


