#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Engine/Application.h>

using namespace Urho3D;

class SyntaxChecker : public Urho3D::Application {
   OBJECT(SyntaxChecker);

public:
   SyntaxChecker(Urho3D::Context*);

   virtual void Setup();
   virtual void Start();
   virtual void Stop() {}

private:
   
   SharedPtr<Context> context;
   String scriptName;
};
