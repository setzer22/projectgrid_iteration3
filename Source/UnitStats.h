#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>

#include "GlobalEnums.h"

using namespace Urho3D;

class UnitStats : public LogicComponent { 
	OBJECT(UnitStats)  

public:

	UnitStats(Context* context);

	static void RegisterObject(Context* context);

    virtual void ApplyAttributes();

    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();

    void AddRef() { /* do nothing */ }
    void ReleaseRef() { /* do nothing */ }

    String GetUnitName() {return unitName;}
 	void SetUnitName(String nunitName) {unitName = nunitName;}

 	Team GetTeam() {return team;}
 	void SetTeam(Team nTeam) {team = nTeam;}

 	MoveType GetMoveType() {return moveType;}
 	void SetMoveType(MoveType nMoveType) {moveType = nMoveType;}

	int GetHP() {return HP;}
 	void SetHP(int nHP) {HP = nHP;}

	int GetMoveRange() {return moveRange;}
 	void SetMoveRange(int nmoveRange) {moveRange = nmoveRange;}

	int GetAttackRange() {return attackRange;}
 	void SetAttackRange(int nattackRange) {attackRange = nattackRange;}

	int GetMinAttackRange() {return minAttackRange;}
 	void SetMinAttackRange(int nminAttackRange) {minAttackRange = nminAttackRange;}
 	
 	int GetActionPoints() {return actionPoints;}
 	void SetActionPoints(int p) {actionPoints = p;}
  
	String unitName;
	int HP;
	int moveRange;
	int attackRange;
	int minAttackRange;
    int actionPoints;
	Team team;
	MoveType moveType;

private:

	String _attr_unitTeam;
	String _attr_moveType;
	
};