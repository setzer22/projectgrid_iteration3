#include "UnitStats.h"
#include <iostream>
#include <assert.h>

using namespace std;

UnitStats::UnitStats(Context* context) : LogicComponent(context) {
    _attr_unitTeam = "ALLY";
    _attr_moveType = "WALK";
}

void UnitStats::RegisterObject(Context* context) {
	context->RegisterFactory<UnitStats>("Unit");
	ATTRIBUTE("Name", String, unitName, "No name", AM_DEFAULT);
	ATTRIBUTE("HP", int,  HP, 0, AM_DEFAULT);
	ATTRIBUTE("Move Range", int,  moveRange, 0, AM_DEFAULT);
	ATTRIBUTE("Max Attack Range", int,  attackRange, 0, AM_DEFAULT);
	ATTRIBUTE("Min Attack Range", int,  minAttackRange, 0, AM_DEFAULT);
	ATTRIBUTE("Team", String,  _attr_unitTeam, "ALLY", AM_DEFAULT);
	ATTRIBUTE("Move Type", String, _attr_moveType, "WALK", AM_DEFAULT);
}

void UnitStats::ApplyAttributes() {
	//Ugly but urho doesn't allow enums in the inspector...
	if(_attr_unitTeam == "ENEMY") team = ENEMY;
	else if(_attr_unitTeam == "ALLY") team = ALLY;
	else if(_attr_unitTeam == "OTHERS") team = OTHERS;
	else {
		team = OTHERS;
		cerr << "[Unit][Stats] This Unit's team is not well defined. Please use one of ENEMY, ALLY, OTHERS" << endl;
	}
	if(_attr_moveType == "WALK") moveType = WALK;
	else if(_attr_moveType == "FLY") moveType = FLY;
	else if(_attr_moveType == "GOD") moveType = GOD;
	else if(_attr_moveType == "ATTACK") moveType = ATTACK;
	else {
		moveType = WALK;
		cerr << "[Unit][Stats] This Unit's move type is not well defined. Please use one of WALK, FLY, GOD, ATTACK" << endl;
	}
}

void UnitStats::BindScriptMethods(asIScriptEngine *engine) {
	int r;
	r = engine->RegisterObjectType("UnitStats",0, asOBJ_REF); assert(r >= 0);
	r = engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "UnitStats@ f()", 
		asFUNCTION((RefCast<Component,UnitStats>)), asCALL_CDECL_OBJLAST); assert(r >= 0);
	r = engine->RegisterObjectBehaviour("UnitStats", asBEHAVE_ADDREF, "void f()", asMETHOD(UnitStats,AddRef), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectBehaviour("UnitStats", asBEHAVE_RELEASE, "void f()", asMETHOD(UnitStats,ReleaseRef), asCALL_THISCALL); assert(r >= 0);
	//r = engine->RegisterObjectProperty("String", "String unitName", asOFFSET(UnitStats,unitName)); assert(r >= 0) ;
	//r = engine->RegisterObjectProperty("int", "int HP", asOFFSET(UnitStats,HP)); assert(r >= 0) ;
	//r = engine->RegisterObjectProperty("int", "int moveRange", asOFFSET(UnitStats,moveRange)); assert(r >= 0) ;
	//r = engine->RegisterObjectProperty("int", "int attackRange", asOFFSET(UnitStats,attackRange)); assert(r >= 0) ;
	//r = engine->RegisterObjectProperty("int", "int minAttackRange", asOFFSET(UnitStats,minAttackRange)); assert(r >= 0) ;
	//r = engine->RegisterObjectProperty("Team", "Team team", asOFFSET(UnitStats,team)); assert(r >= 0) ;
	//r = engine->RegisterObjectProperty("MoveType", "MoveType moveType", asOFFSET(UnitStats,moveType)); assert(r >= 0) ;
	r = engine->RegisterObjectMethod("UnitStats", "String GetUnitName()", asMETHOD(UnitStats, GetUnitName), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("UnitStats", "void SetUnitName(String unitName)", asMETHOD(UnitStats, SetUnitName), asCALL_THISCALL);
	r = engine->RegisterObjectMethod("UnitStats", "int GetHP()", asMETHOD(UnitStats, GetHP), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("UnitStats", "void SetHP(int HP)", asMETHOD(UnitStats, SetHP), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("UnitStats", "int GetMoveRange()", asMETHOD(UnitStats, GetMoveRange), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("UnitStats", "void SetMoveRange(int MoveRange)", asMETHOD(UnitStats, SetMoveRange), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("UnitStats", "int GetAttackRange()", asMETHOD(UnitStats, GetAttackRange), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("UnitStats", "void SetAttackRange(int AttackRange)", asMETHOD(UnitStats, SetAttackRange), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("UnitStats", "int GetMinAttackRange()", asMETHOD(UnitStats, GetMinAttackRange), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("UnitStats", "void SetMinAttackRange(int MinAttackRange)", asMETHOD(UnitStats, SetMinAttackRange), asCALL_THISCALL); assert(r >= 0);
	r = engine->RegisterObjectMethod("UnitStats", "Team GetTeam()", asMETHOD(UnitStats, GetTeam), asCALL_THISCALL); assert (r>=0);
	r = engine->RegisterObjectMethod("UnitStats", "void SetTeam(Team nTeam)", asMETHOD(UnitStats, SetTeam), asCALL_THISCALL); assert (r>=0);
	r = engine->RegisterObjectMethod("UnitStats", "MoveType GetMoveType()", asMETHOD(UnitStats, GetMoveType), asCALL_THISCALL); assert (r>=0);
	r = engine->RegisterObjectMethod("UnitStats", "void SetMoveType(MoveType nMoveType)", asMETHOD(UnitStats, SetMoveType), asCALL_THISCALL); assert (r>=0);
    r = engine->RegisterObjectMethod("UnitStats", "void SetActionPoints(int p)", asMETHOD(UnitStats, SetActionPoints), asCALL_THISCALL); assert(r >= 0);
    r = engine->RegisterObjectMethod("UnitStats", "int GetActionPoints()", asMETHOD(UnitStats, GetActionPoints), asCALL_THISCALL); assert(r >= 0);

}
    
void UnitStats::Start() {

}
