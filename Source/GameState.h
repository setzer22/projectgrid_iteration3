#pragma once

#include <Urho3D/Urho3D.h>
#include "DmCommander.h"
#include "DmStateInfo.h"
#include "StateManager.h"

using namespace std;

class DmStateInfo;

class DmCommander;

class StateManager;

class GameState {

public:

	GameState(StateManager* stateManager);

	virtual ~GameState();

	virtual void Init(DmStateInfo* stateInfo, DmCommander* commander) = 0;

	virtual void Update(DmStateInfo* stateInfo, DmCommander* commander);

	virtual void UpdateGUI(DmStateInfo* stateInfo, DmCommander* commander);

	virtual void End(DmStateInfo* stateInfo, DmCommander* commander);

	StateManager* stateManager;

};