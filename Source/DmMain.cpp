#include <iostream>
#include "DmMain.h"
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/IO/FileSystem.h>
#include <Urho3D/Script/Script.h>

#define GLIBCXX_DEBUG

using namespace std;


DmMain::DmMain(Context* context) :
  LogicComponent(context) {
	//TODO What's the initial state?
	stateManager = new StateManager(GetSubsystem<ResourceCache>(),
		GetSubsystem<FileSystem>());
	stateManager->Init();
	__init = false;
}

void DmMain::RegisterObject(Context* context) {
	context->RegisterFactory<DmMain>("DungeonMaster");
}

void DmMain::ApplyAttributes() {
	cout << "[DM][Main] INFO: Starting Dungeon master component" << endl;

	commanderComponent = 
		(DmCommander*)node_->GetComponent("DmCommander");

	if(commanderComponent == NULL) {
		cerr << "[DM][Main] ERROR: Commander component not found in node" << endl;
		return;
	}

	stateComponent = 
		(DmStateInfo*)node_->GetComponent("DmStateInfo");

	if(stateComponent == NULL) {
		cerr << "[DM][Main] ERROR: State component not found in node" << endl;
		return;
	}

	stateComponent->stateDone = true;
	stateComponent->nextState = "Main";

	//Dummy state to avoid null on the first Update.
	stateComponent->currentState = 
		stateManager->getState("Dummy");

	if (stateManager->getState("Dummy") == NULL) {
		cerr << "[DM][Main] ERROR: Wrong next state name (no such script " 
			 << "Dummy.as). Cannot continue";
		return;
	}

	__init = true;
}



void DmMain::Start() {

}

//Potential bottleneck...
void DmMain::Update(float timeStep) {
	if(!__init) return;

	VariantVector parameters;
	parameters.Push(timeStep);
	parameters.Push(stateComponent);
	parameters.Push(commanderComponent);

	if(stateComponent->stateDone) {
		cout << "[DM][Main] INFO: Swapping old state with " << stateComponent->nextState.CString() << endl;
		stateComponent->currentState->Execute(
			"void sEnd(float, DmStateInfo@, DmCommander@)", parameters);

		stateComponent->currentState = 
			stateManager->getState(stateComponent->nextState.CString());

		if (stateComponent->currentState == NULL) {
			cerr << "[DM][Main] ERROR: Wrong next state name (no such script " 
				 << stateComponent->nextState.CString() << ".as). Cannot continue";
			return;
		}

		stateComponent->stateDone = false;
		stateComponent->currentState->Execute(
			"void sInit(float, DmStateInfo@, DmCommander@)", parameters);

	}

	GameState* lastState = stateComponent->currentState;
	stateComponent->currentState->Execute(
		"void sUpdate(float, DmStateInfo@, DmCommander@)", parameters);

	if(lastState != stateComponent->currentState) {
	}
}

void DmMain::BindScriptMethods(asIScriptEngine *engine) {

}