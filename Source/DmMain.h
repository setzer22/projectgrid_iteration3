#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/Script/ScriptFile.h>
#include "DmStateInfo.h"
#include "DmCommander.h"
#include "StateManager.h"

using namespace Urho3D;

typedef ScriptFile GameState;

class DmMain : public LogicComponent { 
	OBJECT(DmMain)  

public:

	void AddRef() { /* do nothing */ }
    void ReleaseRef() { /* do nothing */ }

	DmMain(Context* context);

	static void RegisterObject(Context* context);

    virtual void ApplyAttributes();

    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();

    virtual void Update(float timeStep);

    //onGUI?
  
private:

	DmStateInfo* stateComponent;
	DmCommander* commanderComponent;

	StateManager* stateManager;
	bool __init;
};