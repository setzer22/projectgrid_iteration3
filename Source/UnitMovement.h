#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/Physics/RigidBody.h>
#include "GlobalEnums.h"

#include <Urho3D/Container/Vector.h>

using namespace Urho3D;

class UnitMovement : public LogicComponent { 
	OBJECT(UnitMovement)  

public:

	UnitMovement(Context* context);

	static void RegisterObject(Context* context);

    virtual void ApplyAttributes();

    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();

    virtual void FixedUpdate(float timestep);
    
    virtual void DelayedStart();

    void Move(Vector<Vector3>* new_waypoints);

    void Move(CScriptArray* new_waypoints);

    void LookAt(Directions direction);
    
    bool GetDone() {return done;}

    void HandleNodeCollision(StringHash eventType, VariantMap& eventData);
    
    void UpdateSmUnit();

    void AddRef() { /* do nothing */ }
    void ReleaseRef() { /* do nothing */ }
  
private:
	
	float speed;
	float gravity;
	float positionTreshold;

	bool done;
	bool onGround;
	int waypointIndex;
	Vector<Vector3>* waypoints;

	RigidBody* unitBody;
    Node* smUnitNode;


};