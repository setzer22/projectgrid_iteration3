#include <string>
#include <iostream>
using namespace std;

#define dbgout std::cout<<"[DEBUG] "<<__FILE__<<" at line "<<__LINE__<<": "

#define TESTNULL(VAR,TYPE,COMPONENT,MSG) if(VAR==NULL) {\
    cerr<<"["<<TYPE<<"][" <<COMPONENT<<"] ERROR: "<<MSG<<endl; \
    SetEnabled(false); \
    return; \
}