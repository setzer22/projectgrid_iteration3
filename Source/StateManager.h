#pragma once

#include <Urho3D/Urho3D.h>
#include <map>
#include <Urho3D/Script/ScriptFile.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/IO/FileSystem.h>

using namespace std;
using namespace Urho3D;

typedef ScriptFile GameState;

class StateManager {

public:

	StateManager(ResourceCache* cache, FileSystem* fs);

	~StateManager();

	void Init();

	GameState* getState(string name);

private:

	map<string, SharedPtr<GameState> > stateLibrary;
	const char* GAME_STATES_PATH = "Scripts/GameStates";
	ResourceCache* cache;
	SharedPtr<GameState> hack_ptr;
	FileSystem* fileSystem;

};

