#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Engine/Application.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Container/Ptr.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Engine/DebugHud.h>
#include <AngelScript/angelscript.h>

using namespace Urho3D;

class MainApplication : public Urho3D::Application {
   OBJECT(MainApplication);

public:
   MainApplication(Urho3D::Context*);

   virtual void Setup();
   virtual void Start();
   virtual void Stop() {}
   void CreateConsoleAndDebugHud();
   void CompileMessageCallback(const asSMessageInfo *msg, void *param);

private:
   void onKeyDown(StringHash,  VariantMap&);
   
   SharedPtr<XMLFile> loadScene;
   SharedPtr<Scene> mainScene;
   SharedPtr<Context> _context;
   SharedPtr<Node> cameraNode;

};