#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Container/Vector.h>
#include <Urho3D/Math/Vector2.h>
#include "GlobalEnums.h"
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/Container/Ptr.h>
#include <vector>
#include <iostream>
#include <assert.h>

using namespace Urho3D;
using namespace std;

class PathfinderInfo : public RefCounted {
public:

	void AddRef() { /* do nothing */ }
        void ReleaseRef() { /* do nothing */ }

	PathfinderInfo(Vector<IntVector2>& MoveRange, Vector<IntVector2>& AttackRange, IntVector2 ini, vector<vector<float>>& DistMap, 
	  MoveType UnitMoveType, Team UnitTeam);

	~PathfinderInfo();

	static void BindScriptMethods(asIScriptEngine *engine);

	IntVector2 get_MoveRange(uint i) {assert(0 <= i && i < MoveRange->Size()); return (*MoveRange)[i];}
	uint moveRangeSize() {return (*MoveRange).Size();}

	IntVector2 get_AttackRange(uint i) {assert(0 <= i && i < AttackRange->Size()); return (*AttackRange)[i];}
	uint attackRangeSize() {return (*AttackRange).Size();}

	IntVector2 GetSource() {return source;}

	vector<vector<float>>* DistMap;
	IntVector2 source;
	Vector<IntVector2>* MoveRange;
	Vector<IntVector2>* AttackRange;
	MoveType UnitMoveType;
	Team UnitTeam;

private:

	
	

};