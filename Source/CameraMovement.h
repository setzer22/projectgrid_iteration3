#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>

using namespace Urho3D;

class CameraMovement : public LogicComponent {
    OBJECT(CameraMovement)
    
public:
    
    void AddRef() { /* do nothing */ }
    void ReleaseRef() { /* do nothing */ }
    
    CameraMovement(Context* context);
    
    static void RegisterObject(Context* context);
    
    virtual void ApplyAttributes();
    
    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();
    
    virtual void Update(float timeStep);
    
    void SetUnitNode(Node* unitNode);
    
    void SetShoulderNode(Node* shoulderNode);
    
    void SetMeleeCombat(Node* smUnit, Node* smUnit2);
    
    void SetMode(bool watchMode);
    
private:
    
    void swapFromTo();
    
    //Fields
    WeakPtr<Node> moveFromNode;
    WeakPtr<Node> moveToNode;
    float switchTime;
    float currentTime;
    Vector3 flyPosition;
    bool unitMode; 
    
    //Attributes
    int attr_cameraNode;
    int attr_unitNode;
    
};

