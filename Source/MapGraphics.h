#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Container/Ptr.h>
#include <Urho3D/Resource/Image.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/Container/Vector.h>
#include <Urho3D/IO/Log.h>
#include <vector>

using namespace Urho3D;
using namespace std;

const String identifier = "[Map][Graphics]";
const String GRID_COLORMUL = "grid_ColorMultiplier";
const String GRID_DIM = "grid_Dimensions";
const String GRID_E = "grid_GridWidth";
const String GRID_ORIGIN = "grid_Origin";
const String GRID_ROUND = "grid_RoundCorner";
const String GRID_S = "grid_SquareSize";
const String GRID_TEXSIZE = "grid_TexSize";

class MapGraphics : public LogicComponent {
	OBJECT(MapGraphics)

public:

	void AddRef() { /* do nothing */ }
    void ReleaseRef() { /* do nothing */ }

	MapGraphics(Context* context);

	~MapGraphics();

	static void RegisterObject(Context* context);

	/// Perform post-load after deserialization. Acquire the components from the scene nodes.
    virtual void ApplyAttributes();

	static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();

	void PaintTile (const Color& color, int x, int y);

	void PaintTiles_script(const Color& color, CScriptArray* positions);

	void ResetTile (int x, int y);
    
    //The current state of the map is set as the current map 
    //for reset until popped. This can be done many times
    void PushColors();
    
    //Removes the last pushed color map only if there was one
    void PopColors();
    
	void ClearMap ();

	void Apply ();

    void SetVisible (bool value);
    

private:
    SharedPtr<Image> colorData;
    
    Vector<SharedPtr<Image>> colorStack;
    
    SharedPtr<Texture2D> terrainTexture;
    
    SharedPtr<Material> terrainMaterial;
    
	String attr_mapImagePath;
    
	IntVector2 grid_dim;
    
	float grid_s;
    
    Vector3 origin;
    
	Vector2 grid_origin;
    
	float grid_round;
    
	float grid_e;
    
	float grid_colormul;
    
	bool verbose;
};