#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Script/APITemplates.h>

enum MoveType {
	WALK, FLY, GOD, ATTACK
};

enum Team {
	ALLY, ENEMY, OTHERS
};

#define TEAM_COUNT 3

enum ActionType {
	MOVE, HARM, FREE
};

enum Directions {
	NORTH, SOUTH, EAST, WEST
};

static void GlobalEnumsBindScriptMethods(asIScriptEngine* engine) {
	int r = 0;
	r = engine->RegisterEnum("MoveType"); assert( r >= 0 );
	r = engine->RegisterEnumValue("MoveType", "WALK", 0); assert( r >= 0 );
	r = engine->RegisterEnumValue("MoveType", "FLY", 1); assert( r >= 0 );
	r = engine->RegisterEnumValue("MoveType", "GOD", 2); assert( r >= 0 );
	r = engine->RegisterEnumValue("MoveType", "ATTACK", 3); assert( r >= 0 );
	
	r = engine->RegisterEnum("Team"); assert( r >= 0 );
	r = engine->RegisterEnumValue("Team", "ALLY", 0); assert( r >= 0 );
	r = engine->RegisterEnumValue("Team", "ENEMY", 1); assert( r >= 0 );
	r = engine->RegisterEnumValue("Team", "OTHERS", 2); assert( r >= 0 );
	
	r = engine->RegisterEnum("ActionType"); assert( r >= 0 );
	r = engine->RegisterEnumValue("ActionType", "MOVE", 0); assert( r >= 0 );
	r = engine->RegisterEnumValue("ActionType", "HARM", 1); assert( r >= 0 );
	r = engine->RegisterEnumValue("ActionType", "FREE", 2); assert( r >= 0 );
	
	r = engine->RegisterEnum("Directions"); assert( r >= 0 );
	r = engine->RegisterEnumValue("Directions", "NORTH", 0); assert( r >= 0 );
	r = engine->RegisterEnumValue("Directions", "SOUTH", 1); assert( r >= 0 );
	r = engine->RegisterEnumValue("Directions", "EAST", 2); assert( r >= 0 );
	r = engine->RegisterEnumValue("Directions", "WEST", 3); assert( r >= 0 );
}

