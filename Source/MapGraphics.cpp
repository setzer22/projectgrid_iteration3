#include <iostream>
#include "MapGraphics.h"
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Graphics/IndexBuffer.h>
#include <Urho3D/Graphics/Terrain.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/Engine/DebugHud.h>
#include <iostream>
#include "Debug.h"

MapGraphics::MapGraphics(Context* context) : LogicComponent(context) {
    
}

MapGraphics::~MapGraphics() {
	//if(currentData != NULL) delete currentData;
}

void MapGraphics::RegisterObject(Context* context) {
	context->RegisterFactory<MapGraphics>("Map");

	ATTRIBUTE("Verbose", bool, verbose, 0, AM_DEFAULT);
	//ATTRIBUTE("Terrain Node", int,  terrainNodeId, 0, AM_DEFAULT | AM_NODEID);
	ATTRIBUTE("Map Image", String,  attr_mapImagePath, "", AM_DEFAULT);

	//Map attributes
	ATTRIBUTE("Grid grid_dim", IntVector2,  grid_dim, 0, AM_DEFAULT);
	ATTRIBUTE("Grid origin", Vector2, grid_origin, 0.5f, AM_DEFAULT);
	ATTRIBUTE("Grid square size", float, grid_s, 0.5f, AM_DEFAULT);
	ATTRIBUTE("Grid thickness", float, grid_e, 0.5f, AM_DEFAULT);
	ATTRIBUTE("Grid round corer", float, grid_round, 1.1f, AM_DEFAULT);
	ATTRIBUTE("Grid transparency", float, grid_colormul, 0.5f, AM_DEFAULT);

}

void MapGraphics::Start() {
	if(verbose)cout << "[Map]:[GraphicsComponent] INFO: Starting map graphics component." << endl;
}

void MapGraphics::ApplyAttributes() {

	if(verbose)cout << "[Map]:[GraphicsComponent] INFO: Loading map graphics component." << endl;

	Scene* scene = GetScene();
	ResourceCache* cache = GetSubsystem<ResourceCache>();

	
	colorData = cache->GetResource<Image>(attr_mapImagePath);
	if(colorData == NULL) {
		std::cerr << "[Map]:[GraphicsComponent] ERROR: Reload failed. Map image not found." << endl;
		SetEnabled(false); return;
	}
	
	colorStack.Clear();
	colorStack.Push(colorData);

    if(!scene->GetChild("Terrain")) {
        std::cerr << "[Map]:[GraphicsComponent] ERROR: Terrain node not found on scene." << endl;
        SetEnabled(false); return;
    }
	Terrain* terrain = (Terrain*)(scene->GetChild("Terrain")->GetComponent("Terrain"));
	if(terrain == NULL) {
		std::cerr << "[Map]:[GraphicsComponent] ERROR: Terrain node doesn't have a Terrain component." << endl;
        SetEnabled(false); return;
	}

	terrainMaterial = terrain->GetMaterial();
	if(terrainMaterial == NULL) {
		std::cerr << "[Map]:[GraphicsComponent] ERROR: Terrain doesn't appear to have a material." << endl;
        SetEnabled(false); return;
	}
	terrainTexture = (Texture2D*)(terrainMaterial->GetTexture(TextureUnit(4)));
	terrainTexture->SetData(colorData);

	terrainMaterial->SetShaderParameter(GRID_S, grid_s);
	terrainMaterial->SetShaderParameter(GRID_DIM, Variant(Vector2(grid_dim.x_, grid_dim.y_)));
	terrainMaterial->SetShaderParameter(GRID_E, grid_e);
	terrainMaterial->SetShaderParameter(GRID_ORIGIN, Variant(grid_origin));
	terrainMaterial->SetShaderParameter(GRID_COLORMUL, grid_colormul);
	terrainMaterial->SetShaderParameter(GRID_ROUND, grid_round);
	terrainMaterial->SetShaderParameter(GRID_TEXSIZE, Variant(Vector2( colorData->GetWidth() , colorData->GetHeight() )));
}

void MapGraphics::PaintTile (const Color& color, int x, int y) {
	if(!IsEnabled()) {cerr << "[Map]:[Graphics] ERROR: This component couldn't initialize or is disabled." << endl; return;}
	if(x >= grid_dim.x_ || x < 0 || y >= grid_dim.y_ || y < 0) {
		if(verbose)cerr << "[Map][Graphics] ERROR: Painting a tile in an invalid position";
		return;
	}
	if(verbose)cout << "[Map]:[Graphics] INFO: Painting tile " << x << "," << y << " to color " 
		<< color.r_ <<" "<<color.g_<<" "<<color.b_<<" "<<color.a_ << endl;
	int col = (int)(color.r_*255);
	col += (int)(color.g_*255) << 8;
	col += (int)(color.b_*255) << 16;
	col += 0xff << 24;
    int offset = colorData->GetHeight() - grid_dim.y_;
	terrainTexture->SetData(0, x, grid_dim.y_ - y - 1 + offset, 1, 1, &col);
}

void MapGraphics::PaintTiles_script(const Color& color, CScriptArray* positions) {
	for(int i = 0; i < positions->GetSize(); ++i) {
		IntVector2* p = static_cast<IntVector2*>(positions->At(i));
		PaintTile(color, p->x_, p->y_);
	}
}

void MapGraphics::ResetTile (int x, int y) {
    if(!IsEnabled()) {cerr << "[Map]:[Graphics] ERROR: This component couldn't initialize or is disabled." << endl; return;}
	if(x >= colorData->GetWidth() || x < 0 || y >= colorData->GetHeight() || y < 0) {
		if(verbose)cerr << "[Map][Graphics] ERROR: Resetting a tile in an invalid position";
		return;
	}
    int offset = colorData->GetHeight() - grid_dim.y_;
	Color color = colorData->GetPixel(x,grid_dim.y_ - y - 1 + offset);
	if(verbose)cout << "[Map]:[Graphics] INFO: Resetting tile " << x << "," << y << " to color " 
		<< color.r_ <<" "<<color.g_<<" "<<color.b_<<" "<<color.a_ << endl;
	int col = (int)(color.r_*255);
	col += (int)(color.g_*255) << 8;
	col += (int)(color.b_*255) << 16;
	col += 0xff << 24;
	terrainTexture->SetData(0, x, grid_dim.y_ - y - 1 + offset, 1, 1, &col);
}

void MapGraphics::ClearMap () {
    if(!IsEnabled()) {cerr << "[Map]:[Graphics] ERROR: This component couldn't initialize or is disabled." << endl; return;}
	if(verbose)cout << "[Map]:[Graphics] INFO: Clearing map" << endl;
	for (int i = 0; i < grid_dim.x_; ++i) {
		for (int j = 0; j < grid_dim.y_; ++j) {
			ResetTile(i,j);
		}
	}
}

void MapGraphics::SetVisible(bool value) {
    if(!IsEnabled()) {cerr << "[Map]:[Graphics] ERROR: This component couldn't initialize or is disabled." << endl; return;}
    terrainMaterial->SetShaderParameter(GRID_COLORMUL, value?grid_colormul:0);
}

void MapGraphics::PushColors() {
    if(!IsEnabled()) {cerr << "[Map]:[Graphics] ERROR: This component couldn't initialize or is disabled." << endl; return;}
    Image* img = new Image(GetContext());
    img->SetSize(colorData->GetWidth(), colorData->GetHeight(), colorData->GetComponents());
    terrainTexture->GetData(0, img->GetData());//TODO: Hope there's no buffer overflow :D
    colorStack.Push(SharedPtr<Image>(img));
    colorData = img;
}

void MapGraphics::PopColors() {
    if(!IsEnabled()) {cerr << "[Map]:[Graphics] ERROR: This component couldn't initialize or is disabled." << endl; return;}
    if(colorStack.Size() <= 1) return;
    cout << "[Map][Graphics] WARNING: Tried to pop empty stack. Request ignored." << endl;
    colorStack.Pop();
    colorData = colorStack.Back();
    ClearMap();
}



//Script bindings
void MapGraphics::BindScriptMethods(asIScriptEngine *engine) {
	engine->RegisterObjectType("MapGraphics",0, asOBJ_REF);
	engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "MapGraphics@ f()", 
		asFUNCTION((RefCast<Component,MapGraphics>)), asCALL_CDECL_OBJLAST);
	engine->RegisterObjectBehaviour("MapGraphics", asBEHAVE_ADDREF, "void f()", asMETHOD(MapGraphics,AddRef), asCALL_THISCALL);
	engine->RegisterObjectBehaviour("MapGraphics", asBEHAVE_RELEASE, "void f()", asMETHOD(MapGraphics,ReleaseRef), asCALL_THISCALL);
	engine->RegisterObjectMethod("MapGraphics", "void PaintTile(const Color &in color, int x, int y)", asMETHOD(MapGraphics, PaintTile), asCALL_THISCALL);
	engine->RegisterObjectMethod("MapGraphics", "void PaintTiles(const Color &in color, Array<IntVector2>@+)", asMETHOD(MapGraphics, PaintTiles_script), asCALL_THISCALL);
	engine->RegisterObjectMethod("MapGraphics", "void ResetTile(int x, int y)", asMETHOD(MapGraphics, ResetTile), asCALL_THISCALL);
	engine->RegisterObjectMethod("MapGraphics", "void ClearMap()", asMETHOD(MapGraphics, ClearMap), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapGraphics", "void PushColors()", asMETHOD(MapGraphics, PushColors), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapGraphics", "void PopColors()", asMETHOD(MapGraphics, PopColors), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapGraphics", "void SetVisible(bool value)", asMETHOD(MapGraphics, SetVisible), asCALL_THISCALL);
}