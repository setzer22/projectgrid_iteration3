#include "UnitMovement.h"

#include <iostream>
#include <Urho3D/IO/MemoryBuffer.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Physics/PhysicsEvents.h>
#include <Urho3D/Physics/PhysicsWorld.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>
#include <Urho3D/Physics/PhysicsEvents.h>

using namespace Urho3D;
using namespace NodeCollision;
using namespace std;

//TODO: The treshold method could be improved... 

UnitMovement::UnitMovement(Context* context) : LogicComponent(context) {
	speed = 1.0f; 
	gravity = -1.0f; 
	positionTreshold = 1.0f;
	done = true;
	waypointIndex = 0;
	waypoints = NULL;
	onGround = false;

	SetUpdateEventMask(USE_FIXEDUPDATE);
}

void UnitMovement::RegisterObject(Context* context) {
    context->RegisterFactory<UnitMovement>("Unit");

    ATTRIBUTE("Speed", float, speed, 1.0f, AM_DEFAULT);
    ATTRIBUTE("Gravity", float, gravity, -1.0f, AM_DEFAULT);
    ATTRIBUTE("Position Treshold", float, positionTreshold, 1.0f, AM_DEFAULT);
}
    
void UnitMovement::Start() {
    cout << "[Unit][Movement] INFO: Starting unit movement component" << endl;
	unitBody = (RigidBody*) node_->GetComponent("RigidBody");
	if(unitBody == NULL) {
	    cerr << "[Unit][Movement] ERROR: No rigidbody set up in this node, cannot continue" << endl;
	    return;
	}
    
    SubscribeToEvent(GetNode(), E_NODECOLLISION, HANDLER(UnitMovement, HandleNodeCollision));
}

void UnitMovement::DelayedStart() {
    smUnitNode = GetScene()->GetNode(node_->GetVar("SmUnitId").GetInt());
    cout << "ID is " << node_->GetVar("SmUnitId").GetInt() << endl;
    if(smUnitNode == NULL) {
        cerr << "[Unit][Movement] ERROR: No SmUnit node found or Id not correctly set" << endl;
        return;
    }
    UpdateSmUnit();
}

void UnitMovement::FixedUpdate(float timestep) {
	Vector3 moveDirection (0,0,0);
	if(!done) {
		Vector3 direction = (*waypoints)[waypointIndex] - unitBody->GetPosition();
		direction.y_ = 0;
		Vector3 up (0,1,0);
		Quaternion rot; rot.FromLookRotation(direction, up);
		unitBody->SetRotation(rot);
		if(direction.Length() < positionTreshold) {
			unitBody->SetPosition((*waypoints)[waypointIndex]+Vector3(0,unitBody->GetPosition().y_,0));
			++waypointIndex;
		}
		direction.Normalize();
		moveDirection = direction;
		if(waypointIndex == waypoints->Size()) {
			done = true;
            UpdateSmUnit();
		}
	}
	unitBody->SetLinearVelocity(10*speed*moveDirection*timestep);
	onGround = false;
}

void UnitMovement::Move(CScriptArray* new_waypoints) {
	waypoints = new Vector<Vector3>(0);
	for(uint i = 0; i < new_waypoints->GetSize(); ++i) {
		waypoints->Push(*(static_cast<Vector3*>(new_waypoints->At(i))));
	} 

	waypointIndex = 0;
	done = false;
}

void UnitMovement::Move(Vector<Vector3>* new_waypoints) {
	waypoints = new_waypoints;
	waypointIndex = 0;
	done = false;
}

void UnitMovement::UpdateSmUnit() {
    smUnitNode->SetPosition(node_->GetPosition());
    smUnitNode->SetRotation(node_->GetRotation());
}

void UnitMovement::LookAt(Directions direction) {
	Quaternion rot;
	switch (direction) {
			case NORTH: 
				rot.FromLookRotation(Vector3(0,0,1.0f));
				unitBody->SetRotation(rot);
				break;
			case SOUTH:
				rot.FromLookRotation(Vector3(0,0,-1.0f));
				unitBody->SetRotation(rot);
				break;
			case EAST:
				rot.FromLookRotation(Vector3(1.0f,0,0));
				unitBody->SetRotation(rot);
				break;
			case WEST:
				rot.FromLookRotation(Vector3(-1.0,0,0));
				unitBody->SetRotation(rot);
				break;
		}
    UpdateSmUnit();
}

void UnitMovement::HandleNodeCollision(StringHash eventType, VariantMap& eventData) {

	MemoryBuffer contacts(eventData[P_CONTACTS].GetBuffer());
    while (!contacts.IsEof()) {

         Vector3 contactPosition = contacts.ReadVector3();
         Vector3 contactNormal = contacts.ReadVector3();
         float contactDistance = contacts.ReadFloat();
         float contactImpulse = contacts.ReadFloat();
         
         // If contact is below node center and mostly vertical, assume it's a ground contact
         if (contactPosition.y_ < (node_->GetPosition().y_ + 1.0f)) {
             float level = Abs(contactNormal.y_);
             if (level > 0.75)
                 onGround = true;
    	}
	}  
}

void UnitMovement::ApplyAttributes() {}

void UnitMovement::BindScriptMethods(asIScriptEngine *engine) {
	engine->RegisterObjectType("UnitMovement",0, asOBJ_REF);
	engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "UnitMovement@ f()", 
		asFUNCTION((RefCast<Component,UnitMovement>)), asCALL_CDECL_OBJLAST);
	engine->RegisterObjectBehaviour("UnitMovement", asBEHAVE_ADDREF, "void f()", asMETHOD(UnitMovement,AddRef), asCALL_THISCALL);
	engine->RegisterObjectBehaviour("UnitMovement", asBEHAVE_RELEASE, "void f()", asMETHOD(UnitMovement,ReleaseRef), asCALL_THISCALL);	
	engine->RegisterObjectMethod("UnitMovement", "void LookAt(Directions direction)", asMETHOD(UnitMovement, LookAt), asCALL_THISCALL);
	engine->RegisterObjectMethod("UnitMovement", "void Move(Array<Vector3>@+ new_waypoints)", asMETHODPR(UnitMovement, Move, (CScriptArray*), void), asCALL_THISCALL);
    engine->RegisterObjectMethod("UnitMovement", "bool GetDone()", asMETHOD(UnitMovement, GetDone), asCALL_THISCALL);
    engine->RegisterObjectMethod("UnitMovement", "void UpdateSmUnit()", asMETHOD(UnitMovement, UpdateSmUnit), asCALL_THISCALL);
}
