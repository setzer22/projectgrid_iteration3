#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Container/Ptr.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>
#include "GlobalEnums.h"
#include <Urho3D/Script/ScriptFile.h>

using namespace Urho3D;

typedef ScriptFile GameState;

class DmStateInfo : public LogicComponent { 
	OBJECT(DmStateInfo)  

public:

	void AddRef() { /* do nothing */ }
    void ReleaseRef() { /* do nothing */ }

	DmStateInfo(Context* context);

	static void RegisterObject(Context* context);

    virtual void ApplyAttributes();

    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();

    virtual void Update();
    
    virtual void DelayedStart();

    void Push(Variant v);

    Variant Pop();
    
    Variant StackAt(int i);
    
    Node* GetUnit(unsigned int i, Team team) {
        return Teams[team][i];
    }
    
    int TeamSize(Team team) {
        return Teams[team].Size();
    }
    
    //Properties
    Vector<Vector<Node*>> Teams;

	Team currentTeam;

	GameState* currentState;

	SharedPtr<Node> currentSelectedUnit;

	VariantVector scriptStack;

	IntVector2 currentSelectedTile;

	String currentSelectedAction;

	int currentSelectedActionIndex;

	bool stateDone; 

	String nextState;
  
private:



};
