#include "MapMouseToTile.h"
#include <iostream>
#include "MapProperties.h"
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Scene/Scene.h>
#include "Debug.h"
#include <Urho3D/Graphics/DebugRenderer.h>

using namespace std;

MapMouseToTile::MapMouseToTile(Context* context) : LogicComponent(context) {

}

void MapMouseToTile::RegisterObject(Context* context) {
	context->RegisterFactory<MapMouseToTile>("Map");
	ATTRIBUTE("Map origin point", Vector3, sensorOrigin, 0, AM_DEFAULT);
	ATTRIBUTE("Map end point", Vector3,  sensorEnd, 0, AM_DEFAULT);
	ATTRIBUTE("Camera node", int, cameraNode, 0, AM_DEFAULT | AM_NODEID);

}

void MapMouseToTile::ApplyAttributes() {
	MapProperties* properties = (MapProperties*)node_->GetComponent("MapProperties");
	if(properties == NULL) {
		cerr << "[Map][Mouse]" << " at line " << __LINE__ << "ERROR: Map properties not initialised, cannot start" << endl;
		return;
	}
	int w = properties->Width();
	int h = properties->Height();

	float tileSizeX = (sensorEnd.x_ - sensorOrigin.x_) / w;
	float tileSizeY = (sensorEnd.z_ - sensorOrigin.z_) / h;

	if(tileSizeX != tileSizeY) {
		cerr << "[Map][Mouse]" << " at line " << __LINE__ << "ERROR: Tiles in your map are not square. This component won't work properly." << endl;
	}

	tileSize = tileSizeX;

	Node* camNode = (Node*)GetScene()->GetNode(cameraNode);
	if(camNode == NULL) {
		cerr << "[Map][Mouse] ERROR: The camera node set doesn't exist" << endl;
		return;
	}
	camera = (Camera*)camNode->GetComponent("Camera");
	if(camera == NULL) {
		cerr << "[Map][Mouse] ERROR: The camera node doesn't have a camera component on it" << endl; 
		return;
	}
	//TODO: Find a better way. node 1 for the scene shouldn't be assumed...
	octree = (Octree*)GetScene()->GetNode(1)->GetComponent("Octree"); //only one octree in the scene is assumed.
	if(octree == NULL) {
		cerr << "[Map][Mouse] ERROR: The scene doesn't have an octree. Cannot continue" << endl;
		return;
	}
}
    
void MapMouseToTile::Start() {
		
}

IntVector2 MapMouseToTile::WorldToMap(Vector2 normalizedScreenPos) {
	PODVector<RayQueryResult> result;
	Ray ray = camera->GetScreenRay(normalizedScreenPos.x_,normalizedScreenPos.y_);
	RayOctreeQuery q (result, ray, RAY_TRIANGLE, M_INFINITY, DRAWABLE_GEOMETRY, 1);
	octree->RaycastSingle(q);
	if(result.Size() >= 1) {
		return WorldToMap(result[0].position_);
	}
	return IntVector2(-1,-1);
}

IntVector2 MapMouseToTile::WorldToMap(Vector3 position) {
	if(position.x_ > sensorEnd.x_ || position.z_ > sensorEnd.z_ || 
		position.x_ < sensorOrigin.x_ || position.z_ < sensorOrigin.z_) {
		//should never happen, but still... some bad rounding might lead to this.
		return IntVector2(-1,-1);
	}
	int mapX = (int)((position.x_ - sensorOrigin.x_) / tileSize);
	int mapY = (int)((position.z_ - sensorOrigin.z_) / tileSize);
	return IntVector2(mapX, mapY);
}

Vector3 MapMouseToTile::MapToWorld(IntVector2 mapPosition) {
	return Vector3(sensorOrigin.x_+(mapPosition.x_*tileSize)+(tileSize*0.5f),0.0f,sensorOrigin.z_+(mapPosition.y_*tileSize)+(tileSize*0.5f));
}

void MapMouseToTile::BindScriptMethods(asIScriptEngine *engine) {

	engine->RegisterObjectType("MapMouseToTile",0, asOBJ_REF);
	engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "MapMouseToTile@ f()", 
		asFUNCTION((RefCast<Component,MapMouseToTile>)), asCALL_CDECL_OBJLAST);
	engine->RegisterObjectBehaviour("MapMouseToTile", asBEHAVE_ADDREF, "void f()", asMETHOD(MapMouseToTile,AddRef), asCALL_THISCALL);
	engine->RegisterObjectBehaviour("MapMouseToTile", asBEHAVE_RELEASE, "void f()", asMETHOD(MapMouseToTile,ReleaseRef), asCALL_THISCALL);
	engine->RegisterObjectMethod("MapMouseToTile", "IntVector2 WorldToMap(Vector2 normalizedScreenPos)", 
		asMETHODPR(MapMouseToTile, WorldToMap, (Vector2), IntVector2), asCALL_THISCALL);
	engine->RegisterObjectMethod("MapMouseToTile", "IntVector2 WorldToMap(Vector3 position)", 
		asMETHODPR(MapMouseToTile, WorldToMap, (Vector3), IntVector2), asCALL_THISCALL);
	engine->RegisterObjectMethod("MapMouseToTile", "Vector3 MapToWorld(IntVector2 mapPosition)", asMETHOD(MapMouseToTile, MapToWorld), asCALL_THISCALL);
 
}

