#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/UI/Window.h>
#include <Urho3D/UI/UIElement.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/Container/Ptr.h>

using namespace Urho3D;

class GUIMain: public LogicComponent {
    OBJECT(GUIMain)
    
public:
    
    const String FONT_NAME = "Fonts/Anonymous Pro.ttf";
    
    GUIMain(Context* context);
    
    static void RegisterObject(Context* context);
    
    virtual void ApplyAttributes();
    
    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();
    
    void SetUnitVisible(bool visible);
    
    void SetTileVisible(bool visible);
    
    void SetActionVisible(bool visible);
    
    void SetUnit(String name, int HP, int moveRange, int atkRange);
    
    void SetTile(IntVector2 coords, String name, float cost);
    
    void AttackPressed(StringHash eventType, VariantMap& eventData);
    
    void AddRef() {/* do nothing */}
    void ReleaseRef() {/* do nothing */}
    
    
private:
    
    /**The GUI windows*/
    SharedPtr<Window> unitWindow, tileWindow, actionWindow;
    
    /**Toggle the visibility for the widows*/
    bool unitVisible, tileVisible, actionVisible;
    
    SharedPtr<Text> unitName, tileName, unitHP, unitRng, unitAtkRng,
         tileInfo, tileCoords, tileCost;
         
    SharedPtr<XMLFile> style;
    
    Context* context;
         
    void initText(SharedPtr<Text>& text, String name, String l_text);
    
};