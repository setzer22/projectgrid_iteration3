#include "SyntaxChecker.h"
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Script/ScriptFile.h>
#include <Urho3D/Script/Script.h>
#include "CustomComponents.h"

using namespace Urho3D;

DEFINE_APPLICATION_MAIN(SyntaxChecker)

SyntaxChecker::SyntaxChecker(Urho3D::Context* context) : Application(context){
    this->context = context;
}

void SyntaxChecker::Setup(){
    engineParameters_["Headless"] = true;
    
    context_->RegisterSubsystem(new Script(context_));
    asIScriptEngine* engine = GetSubsystem<Script>()->GetScriptEngine();
    
    CustomComponents::RegisterObjects(context_);
    CustomComponents::BindScriptMethods(engine);
    
    const Vector<String>& arguments = GetArguments();
    scriptName = arguments[0];

}

void SyntaxChecker::Start(){
    
    ScriptFile* e = context->GetSubsystem<ResourceCache>()->
        GetResource<ScriptFile>("Scripts/"+scriptName);
        
    engine_->Exit();
                
}