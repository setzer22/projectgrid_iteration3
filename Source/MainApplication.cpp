#include "MainApplication.h"
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/Core/Main.h>
#include <Urho3D/UI/Text.h>
#include "CustomComponents.h"
#include <Urho3D/Graphics/Renderer.h>
#include <iostream>
#include <Urho3D/Engine/Console.h>


//Stack trace debug
#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

void stacktrace_handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

//-----------------

using namespace std;
using namespace Urho3D;

DEFINE_APPLICATION_MAIN(MainApplication)

MainApplication::MainApplication(Urho3D::Context* context) : Application(context){
   _context = context;
}

void MainApplication::Setup(){

   //Stack trace debug
   signal(SIGSEGV, stacktrace_handler);
   //----------------------

   cout << "Hello Setup" << endl;
   engineParameters_["FullScreen"] = false;
}

void MainApplication::Start(){
  cout << "Hello Start" << endl;
  SubscribeToEvent(E_KEYDOWN, HANDLER(MainApplication,onKeyDown));


  //Initialize script subsystem
  context_->RegisterSubsystem(new Script(context_));
  asIScriptEngine* scriptEngine = GetSubsystem<Script>()->GetScriptEngine();
  scriptEngine->SetMessageCallback(asMETHOD(MainApplication,CompileMessageCallback), 0, asCALL_THISCALL);
  CustomComponents::RegisterObjects(context_);
  CustomComponents::BindScriptMethods(scriptEngine);
    
  CreateConsoleAndDebugHud();

  ResourceCache* cache = GetSubsystem<ResourceCache>();
  mainScene = new Scene(context_);

  loadScene = cache->GetResource<XMLFile>
    ("Scenes/DemoMap.xml");
  mainScene->LoadXML(loadScene->GetRoot());
  if(mainScene == NULL) 
    cerr << "Couldn't get scene, is null" << endl;

  GetSubsystem<Script>()->SetDefaultScene(mainScene);

  cout << "Hello Start" << endl;
  cerr << "Create console and debug hud" << endl;
   

  //Get the main camera
  cameraNode = mainScene->GetChild("Camera");

  //Setup the viewport
  Renderer* renderer = GetSubsystem<Renderer>();

  GetSubsystem<Input>()->SetMouseVisible(true);

  // Set up a viewport to the Renderer subsystem so that the 3D scene can be seen
  SharedPtr<Viewport> viewport(new Viewport(context_,
    mainScene, cameraNode->GetComponent<Camera>()));
  renderer->SetViewport(0, viewport);   
}

void MainApplication::onKeyDown(StringHash event, VariantMap& data){
    if(data["Key"] == Urho3D::KEY_ESC)
        engine_->Exit();
}

 void MainApplication::CreateConsoleAndDebugHud()
{
    // Get default style
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    XMLFile* xmlFile = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");
    cout << (xmlFile==NULL) << " <- isnull?" << endl;

    // Create debug HUD.                                                                                                                  
    DebugHud* debugHud = engine_->CreateDebugHud();
    debugHud->SetDefaultStyle(xmlFile);
}

void MainApplication::CompileMessageCallback(const asSMessageInfo* msg, void* param) {
    const char *type = "ERR ";
    if( msg->type == asMSGTYPE_WARNING ) 
        type = "WARN";
    else if( msg->type == asMSGTYPE_INFORMATION ) 
        type = "INFO";
    printf("%s (%d, %d) : %s : %s\n", msg->section, msg->row, msg->col, type, msg->message);
}
