#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/Input/Input.h>
#include "MapMouseToTile.h"
#include "MapData.h"
#include "MapGraphics.h"
#include "MapPathfinder.h"
#include "MapDetail.h"
#include "CameraMovement.h"
#include "GUIMain.h"
#include "DmStateInfo.h"

using namespace Urho3D;

class DmCommander : public LogicComponent { 
	OBJECT(DmCommander)  

public:

    DmCommander(Context* context);

    static void RegisterObject(Context* context);

    virtual void ApplyAttributes();

    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();

    virtual void Update();

    IntVector2 GetMouseTile();

    bool QueryMouseTile(IntVector2& tile);

    void PaintInRange(PathfinderInfo* pathInfo, Color& color,
      bool attackRange = false);
    
    void SetUnitGUI(Node* unit);
    
    void SetTileGUI(IntVector2 tile);
    
    Team intToTeam(int t) {return static_cast<Team>(t);}
    
    int teamToInt(Team t) {return static_cast<int>(t);}
    
    ///Returns a variant containing a variant vector of all the units in range
    ///from this pathInfo
    Variant GetUnitsInRange(PathfinderInfo* pathInfo, bool attackRange = false, 
                            bool excludeOrigin = true);
    
    Variant GetUnitsInTeam(DmStateInfo* state, Team team, int minActionPoints = -1);

    MapMouseToTile* GetMouse() {
        return mouseComponent;
    }

    MapGraphics* GetGraphics() {
        return graphicsComponent;
    }

    MapPathfinder* GetPathfinder() {
        return pathfinderComponent;
    }

    MapData* GetData() {
        return dataComponent;
    }
    
    MapDetail* GetDetail() {
        return detailComponent;
    }
    
    CameraMovement* GetCamera() {
        return cameraComponent;
    }
    
    GUIMain* GetGUI() {
        return guiComponent;
    }
    
    void AddRef() { /* do nothing */ }
    void ReleaseRef() { /* do nothing */ }
  
private:

    MapMouseToTile* mouseComponent;
    MapGraphics* graphicsComponent;
    MapPathfinder* pathfinderComponent;
    MapData* dataComponent;
    MapDetail* detailComponent;
    CameraMovement* cameraComponent;
    GUIMain* guiComponent;

    IntVector2 lastTile;

};