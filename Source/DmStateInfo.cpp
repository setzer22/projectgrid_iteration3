#include "DmStateInfo.h"
#include <Urho3D/Scene/Scene.h>
#include <assert.h>
#include "UnitStats.h"
#include "Debug.h"

DmStateInfo::DmStateInfo(Context* context) :
  LogicComponent(context),
  Teams(TEAM_COUNT) {

  	scriptStack = VariantVector(0);
}

void DmStateInfo::RegisterObject(Context* context) {
	context->RegisterFactory<DmStateInfo>("DungeonMaster");
}

void DmStateInfo::ApplyAttributes() {
    Node* unitsNode = GetScene()->GetChild("Units");
    TESTNULL(unitsNode, "Dm", "State", "The units node is not present in this scene!");
}

void DmStateInfo::DelayedStart() {
    Node* unitsNode = GetScene()->GetChild("Units");
    Vector<SharedPtr<Node>> units = unitsNode->GetChildren();
    for(auto i = units.Begin(); i != units.End(); ++i) {
        UnitStats* s = (*i)->GetComponent<UnitStats>();
        Teams[s->team].Push(*i);
    }
}

void DmStateInfo::BindScriptMethods(asIScriptEngine *engine) {
	int r;
	r = engine->RegisterObjectType("DmStateInfo",0, asOBJ_REF); assert(r>=0);
	r = engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "DmStateInfo@ f()", 
		asFUNCTION((RefCast<Component,DmStateInfo>)), asCALL_CDECL_OBJLAST); assert(r>=0);
	r = engine->RegisterObjectBehaviour("DmStateInfo", asBEHAVE_ADDREF, "void f()", asMETHOD(DmStateInfo,AddRef), asCALL_THISCALL); assert(r>=0);
	r = engine->RegisterObjectBehaviour("DmStateInfo", asBEHAVE_RELEASE, "void f()", asMETHOD(DmStateInfo,ReleaseRef), asCALL_THISCALL); assert(r>=0);
	r = engine->RegisterObjectProperty("DmStateInfo", "Team currentTeam", asOFFSET(DmStateInfo, currentTeam)); assert(r>=0);
	r = engine->RegisterObjectProperty("DmStateInfo", "ScriptFile@ currentState", asOFFSET(DmStateInfo, currentState)); assert(r>=0);
	r = engine->RegisterObjectProperty("DmStateInfo", "Node@ currentSelectedUnit", asOFFSET(DmStateInfo, currentSelectedUnit)); assert(r>=0);
	r = engine->RegisterObjectProperty("DmStateInfo", "String currentSelectedAction", asOFFSET(DmStateInfo, currentSelectedAction)); assert(r>=0);
	r = engine->RegisterObjectProperty("DmStateInfo", "int currentSelectedActionIndex", asOFFSET(DmStateInfo, currentSelectedActionIndex)); assert(r>=0);
	r = engine->RegisterObjectProperty("DmStateInfo", "bool stateDone", asOFFSET(DmStateInfo, stateDone)); assert(r>=0);
	r = engine->RegisterObjectProperty("DmStateInfo", "String nextState", asOFFSET(DmStateInfo, nextState)); assert(r>=0);
	r = engine->RegisterObjectMethod("DmStateInfo", "void Push(Variant v)", asMETHOD(DmStateInfo, Push), asCALL_THISCALL); assert(r>=0);
	r = engine->RegisterObjectMethod("DmStateInfo", "Variant Pop()", asMETHOD(DmStateInfo, Pop), asCALL_THISCALL); assert(r>=0);
    r = engine->RegisterObjectMethod("DmStateInfo", "Variant StackAt(int i)", asMETHOD(DmStateInfo, StackAt), asCALL_THISCALL); assert(r>=0);
    r = engine->RegisterObjectMethod("DmStateInfo", "Node@ GetUnit(int i, Team team)", asMETHOD(DmStateInfo, GetUnit), asCALL_THISCALL); assert(r>=0);
    r = engine->RegisterObjectMethod("DmStateInfo", "int TeamSize(Team team)", asMETHOD(DmStateInfo, TeamSize), asCALL_THISCALL); assert(r>=0);
    
}

void DmStateInfo::Start() {

}

void DmStateInfo::Update() {

}

void DmStateInfo::Push(Variant v) {
	scriptStack.Push(v);
}

Variant DmStateInfo::Pop() {
	Variant v = scriptStack.Back();
	scriptStack.Pop();
	return v;
}

Variant DmStateInfo::StackAt(int i) {
    return scriptStack[scriptStack.Size()-1-i];
}



  
