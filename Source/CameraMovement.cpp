#include "CameraMovement.h"

#include "Debug.h"
#include <Urho3D/Graphics/Camera.h>
#include <iostream>


using namespace std;

CameraMovement::CameraMovement(Context* context) : LogicComponent(context) {

}

void CameraMovement::RegisterObject(Context* context) {
    context->RegisterFactory<CameraMovement>("Camera");
    ATTRIBUTE("Camera fly node id", int, attr_cameraNode, 0, AM_DEFAULT);
    ATTRIBUTE("Unit node id", int, attr_unitNode, 0, AM_DEFAULT);
    ATTRIBUTE("Switch animation time", float, switchTime, 0, AM_DEFAULT);
    ATTRIBUTE("Camera fly position", Vector3, flyPosition, Vector3::ZERO, AM_DEFAULT);
}

void CameraMovement::SetMeleeCombat(Node* smUnit, Node* smUnit2) {
    /*Node* smUnit = attacking->GetChild("Shoulder");
    TESTNULL(smUnit, "Camera", "Movement", "Attacking Unit node doesn't have a child named \"Shoulder\"")

    Node* smUnit2 = attacked->GetChild("Shoulder");
    TESTNULL(smUnit2, "Camera", "Movement", "Attacked Unit node doesn't have a child named \"Shoulder\"")*/

    Node* midNode = new Node(GetContext());
    midNode->SetPosition((smUnit->GetPosition() + smUnit2->GetPosition())*0.5);
    Vector3 AB = smUnit2->GetPosition() - smUnit->GetPosition();
    Vector3 lookDirection = Vector3(-AB.z_, 0, AB.x_);
    Quaternion rot;
    rot.FromLookRotation(lookDirection, Vector3(0,1,0));
    midNode->SetRotation(rot);

    Node* cameraPos = new Node(GetContext());
    float back = (.65f*AB.Length()) / tan(0.5f*GetComponent<Camera>()->GetFov());
    cameraPos->SetPosition(midNode->GetPosition() - midNode->GetDirection()*back + Vector3(0,3,0));
    cameraPos->LookAt(midNode->GetPosition());
    
    SetShoulderNode(cameraPos);
}

void CameraMovement::ApplyAttributes() {
    SetEnabled(true);

    unitMode = false; //TODO Hack

    moveFromNode = GetScene()->GetNode(attr_cameraNode);
    TESTNULL(moveFromNode, "Movement", "Camera", "Camera fly mode is not set to a valid scene node");

    Node* unit = GetScene()->GetNode(attr_unitNode);
    TESTNULL(unit, "Camera", "Movement", "Unit node is not set to a valid scene node");

    moveToNode = unit->GetChild("Shoulder");
    TESTNULL(moveToNode, "Camera", "Movement", "Unit node doesn't have a child named \"Shoulder\"");
    currentTime = 0;
    if(!unitMode) swapFromTo();
}

void CameraMovement::Update(float timeStep) {
    if(enabled_ == false) return;

    currentTime += timeStep;
    float frac = currentTime / switchTime;
    if(frac >= 1) {
        return;
    }
    else {
        GetNode()->SetPosition(GetNode()->GetPosition().
                               Lerp(moveToNode->GetWorldPosition(),frac));
        GetNode()->SetRotation(GetNode()->GetWorldRotation().
                               Nlerp(moveToNode->GetWorldRotation(),frac, true));
    }
}


void CameraMovement::SetUnitNode(Node* unitNode) {
    Node* shoulder = unitNode->GetChild("Shoulder");
    TESTNULL(shoulder, "Camera", "Movement", "Unit node doesn't have a child named \"Shoulder\"");
    if(unitMode)
        moveToNode = shoulder;
    else
        moveFromNode = shoulder;
}

void CameraMovement::SetShoulderNode(Node* shoulderNode) {
    TESTNULL(shoulderNode, "Camera", "Movement", "Shoulder node is null, ignoring request");
    if(unitMode)
        moveToNode = shoulderNode;
    else
        moveFromNode = shoulderNode;
}


void CameraMovement::SetMode(bool unitmode) {
    if(unitMode != unitmode) {
        swapFromTo();
        unitMode = unitmode;
        currentTime = 0;
    }
}

void CameraMovement::swapFromTo() {
    Node* aux = moveFromNode;
    moveFromNode = moveToNode;
    moveToNode = aux;
}


void CameraMovement::BindScriptMethods(asIScriptEngine *engine) {
    int r;
    r = engine->RegisterObjectType("CameraMovement",0, asOBJ_REF);
    assert (r>=0);
    r = engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "CameraMovement@ f()",
                                        asFUNCTION((RefCast<Component,CameraMovement>)), asCALL_CDECL_OBJLAST);
    assert (r>=0);
    r = engine->RegisterObjectBehaviour("CameraMovement", asBEHAVE_ADDREF, "void f()", asMETHOD(CameraMovement,AddRef), asCALL_THISCALL);
    assert (r>=0);
    r = engine->RegisterObjectBehaviour("CameraMovement", asBEHAVE_RELEASE, "void f()", asMETHOD(CameraMovement,ReleaseRef), asCALL_THISCALL);
    assert (r>=0);
    r = engine->RegisterObjectMethod("CameraMovement", "void SetUnitNode(Node@+ unitNode)", asMETHOD(CameraMovement, SetUnitNode), asCALL_THISCALL);
    assert (r>=0);
    r = engine->RegisterObjectMethod("CameraMovement", "void SetMode(bool watchMode)", asMETHOD(CameraMovement, SetMode), asCALL_THISCALL);
    assert (r>=0);
    r = engine->RegisterObjectMethod("CameraMovement", "void SetShoulderNode(Node@ shoulderNode)", asMETHOD(CameraMovement, SetShoulderNode), asCALL_THISCALL);
    assert (r>=0);
    r = engine->RegisterObjectMethod("CameraMovement", "void SetMeleeCombat(Node@ attacking, Node@ attacked)", asMETHOD(CameraMovement, SetMeleeCombat), asCALL_THISCALL);
    assert (r>=0);
    
}

void CameraMovement::Start() {

}


