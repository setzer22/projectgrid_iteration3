#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/Graphics/Octree.h>

using namespace Urho3D;

class MapMouseToTile : public LogicComponent { 
	OBJECT(MapMouseToTile)  

public:

	void AddRef() { /* do nothing */ }
    void ReleaseRef() { /* do nothing */ }

	MapMouseToTile(Context* context);

	static void RegisterObject(Context* context);

    virtual void ApplyAttributes();

    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();

    IntVector2 WorldToMap(Vector2 normalizedScreenPos);

    IntVector2 WorldToMap(Vector3 worldPosition);

    Vector3 MapToWorld(IntVector2 mapPosition);
  
private:
	
	Vector3 sensorOrigin;
	Vector3 sensorEnd;
	float tileSize;
	SharedPtr<Camera> camera;
	SharedPtr<Octree> octree;
	int cameraNode;			
	
};