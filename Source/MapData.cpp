#include "MapData.h"

#include <string>
#include <iostream>
#include <stdexcept>
#include "Debug.h"
#include "UnitStats.h"
#include "GlobalEnums.h"

MapData::MapData(Context* context) : LogicComponent(context) {

}

void MapData::RegisterObject(Context* context) {
    context->RegisterFactory<MapData>("Map");
}

void MapData::ApplyAttributes() {
    XMLFile* mapFile = propertiesComponent->XmlData();
    if(mapFile == NULL) {
        cerr << "[Map]:[Data] ERROR: No map xml specified, or wrong path. Cannot start." << endl;
        return;
    }

    XMLElement mapNode = mapFile->GetRoot("map");
    String mapData = mapNode.GetChild("data").GetValue();
    
    Vector<int> values = ParseMapData(mapData);

    int width = stoi(mapNode.GetChild("columns").GetValue().CString());
    int height = stoi(mapNode.GetChild("rows").GetValue().CString());

    tileMap = Vector<Vector<Tile>>(height);
    for(int i = 0; i < height; ++i) tileMap[i] = Vector<Tile>(width);

    if(values.Size() != width*height) {
        cerr << "[Map]:[Data] ERROR: The dimensions in the xml file and the map data don't match. Cannot continue." << endl;
        return;
    }

    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            int id = values[i*width+j];
            if(id > 0) {
                XMLElement tileNode = mapNode.SelectSingle("tile[@id="+String(id)+"]");

                tileMap[i][j].name = tileNode.GetChild("name").GetValue();
                tileMap[i][j].cost = stof(tileNode.GetChild("cost").GetValue().CString());
                tileMap[i][j].walkable = (tileNode.GetChild("walkable").GetValue() == "true")? true:false;
                tileMap[i][j].unit = NULL;
                //add more attributes here
            }
            else {
                tileMap[i][j].name = "Empty";
                tileMap[i][j].cost = 1.0f;
                tileMap[i][j].walkable = false;
                tileMap[i][j].unit = NULL;
            }
            
        }
    }
}
   
void MapData::Start() {
    cout << "[Map][Data] INFO: Starting map data component" << endl;
    Scene* scene = GetScene();
    propertiesComponent = (MapProperties*)node_->GetComponent("MapProperties");
    if(propertiesComponent == NULL) {
        cerr << "[Map]:[Data] ERROR: No properties component present. Cannot start." << endl;
        return;
    }
}



Vector<int> MapData::ParseMapData(String & mapData) {
    int current_val = 0; bool read_sth = false;
    Vector<int> values;
    for(auto it = mapData.Begin(); it != mapData.End(); ++it) {
        if( *it >= '0' && *it <= '9' ) {
            current_val = current_val*10 + (int)(*it - '0');
            read_sth = true;
        }
        else if(read_sth) {
            values.Push(current_val);
            current_val = 0;
            read_sth = false;
        }
    }
    if(read_sth) {
        values.Push(current_val);
        current_val = 0;
        read_sth = false;
    }
    return values;
}

void MapData::CheckSafe(int x, int y) {
    if(y < 0 || y >= tileMap.Size() || x < 0 || x >= tileMap[0].Size()) {
        cerr << "[Map][Data] ERROR: Tried to query an out-of-bounds tile ("<<x<<","<<y<<")" << endl;
        cerr << "Size is " << tileMap.Size() << "x" << tileMap[0].Size() << endl;
        throw std::out_of_range("Coordinates are out of range");
    }
}

bool MapData::TileWalkable (int x, int y) {
    CheckSafe(x,y);
    return tileMap [tileMap.Size() - y - 1] [x].walkable;
}

bool MapData::TileWalkable (IntVector2 p) {
    return TileWalkable (p.x_, p.y_);
}

bool MapData::TileWalkable (IntVector2 p, MoveType moveType, Team team) {
    if(tileMap [tileMap.Size() - p.y_ - 1] [p.x_].unit == NULL)
            return TileWalkable (p.x_, p.y_);
    else {
        UnitStats* unit = tileMap[tileMap.Size() - p.y_ - 1][p.x_].unit->GetComponent<UnitStats>();
        switch(moveType) {
            
        case ATTACK :
            return TileWalkable(p.x_, p.y_);

        case WALK :
            return IsTeamCompatible(unit->team, team) &&
                    TileWalkable(p.x_, p.y_);

        case FLY :
            return TileWalkable(p.x_, p.y_);

        case GOD :
            return true;

        default :
            return IsTeamCompatible(unit->team, team) &&
                    TileWalkable(p.x_, p.y_);
        }
    }
}

String MapData::TileName (int x, int y) {
    CheckSafe(x,y);
    return tileMap [tileMap.Size() - y - 1] [x].name;
}

String MapData::TileName (IntVector2 p) {
    return TileName(p.x_, p.y_);
}

float MapData::TileCost (int x, int y) {
    CheckSafe(x,y);
    return tileMap [tileMap.Size() - y - 1] [x].cost;
}

float MapData::TileCost (IntVector2 p) {
    return TileCost(p.x_, p.y_);
}

bool MapData::TileOccupied (int x, int y) {
    CheckSafe(x,y);
    return tileMap [tileMap.Size() - y - 1] [x].unit == NULL;
}

bool MapData::TileOccupied (IntVector2 p) {
    return TileOccupied(p.x_, p.y_);
}

Node* MapData::TileUnit (int x, int y) {
    CheckSafe(x,y);
    return tileMap [tileMap.Size() - y - 1] [x].unit;
}

Node* MapData::TileUnit (IntVector2 p) {
    return TileUnit(p.x_, p.y_);
}

void MapData::SetUnit (Node* unit, int x, int y) {
    CheckSafe(x,y);
    tileMap [tileMap.Size() - y - 1] [x].unit = unit;
}

void MapData::SetUnit (Node* unit, IntVector2 p) {
    SetUnit(unit, p.x_, p.y_);
}   

bool MapData::IsTeamCompatible(Team a, Team b) {
    if (a == b) return true;
    else if ((a == ALLY && b == OTHERS) || (b == ALLY && a == OTHERS)) return true;
    else return false;
}


void MapData::ParseMap () {

}

void MapData::BindScriptMethods(asIScriptEngine *engine) {
    engine->RegisterObjectType("MapData",0, asOBJ_REF);
    engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "MapData@ f()", 
        asFUNCTION((RefCast<Component,MapData>)), asCALL_CDECL_OBJLAST);
    engine->RegisterObjectBehaviour("MapData", asBEHAVE_ADDREF, "void f()", asMETHOD(MapData,AddRef), asCALL_THISCALL);
    engine->RegisterObjectBehaviour("MapData", asBEHAVE_RELEASE, "void f()", asMETHOD(MapData,ReleaseRef), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "bool TileWalkable(int x, int y)", asMETHODPR(MapData, TileWalkable, (int,int), bool), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "bool TileWalkable(IntVector2 p)", asMETHODPR(MapData, TileWalkable, (IntVector2), bool), asCALL_THISCALL);
    //engine->RegisterObjectMethod("MapData", "bool TileWalkable(IntVector2 p, MoveType moveType, Team team)", asMETHOD(MapData, TileWalkable), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "String TileName(int x, int y)", asMETHODPR(MapData, TileName, (int, int), String), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "String TileName(IntVector2 p)", asMETHODPR(MapData, TileName, (IntVector2), String), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "float TileCost(int x, int y)", asMETHODPR(MapData, TileCost, (int,int), float), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "float TileCost(IntVector2 p)", asMETHODPR(MapData, TileCost, (IntVector2), float), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "bool TileOccupied(int x, int y)", asMETHODPR(MapData, TileOccupied, (int,int), bool), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "bool TileOccupied(IntVector2 p)", asMETHODPR(MapData, TileOccupied, (IntVector2), bool), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "Node@+ TileUnit(int x, int y)", asMETHODPR(MapData, TileUnit, (int,int), Node*), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "Node@+ TileUnit(IntVector2 p)", asMETHODPR(MapData, TileUnit, (IntVector2), Node*), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "void SetUnit(Node@+ unit, int x, int y)", asMETHODPR(MapData, SetUnit, (Node*,int,int), void), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapData", "void SetUnit(Node@+ unit, IntVector2 p)", asMETHODPR(MapData, SetUnit, (Node*,IntVector2), void), asCALL_THISCALL);

}
