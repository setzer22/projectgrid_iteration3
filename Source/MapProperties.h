#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/Resource/XMLFile.h>

using namespace Urho3D;

class MapProperties : public LogicComponent { 
	OBJECT(MapProperties)  

public:

	void AddRef() { /* do nothing */ }
	
    void ReleaseRef() { /* do nothing */ }

	MapProperties(Context* context);

	static void RegisterObject (Context* context);

    virtual void ApplyAttributes ();

    static void BindScriptMethods (asIScriptEngine *engine);
    
    virtual void Start ();

    int Width ();

    int Height ();

    XMLFile* XmlData ();

    void SetXmlAttr(ResourceRef value);

    ResourceRef GetXmlAttr() const;
  
private:
	
	SharedPtr<XMLFile> mapXml;

	int width, height;

};