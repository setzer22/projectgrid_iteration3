#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Graphics/AnimatedModel.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/Terrain.h>

using namespace Urho3D;

class MapDetail : public LogicComponent {
    OBJECT(MapDetail)
    
public:
    
    MapDetail(Context* context);
    
    static void RegisterObject(Context* context);
    
    virtual void ApplyAttributes();
    
    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();
    
    void SetHighDetail(bool value);
    
    void AddRef() {/* do nothing */}
    void ReleaseRef() {/* do nothing */}
    
private:
    
    Node* detailsNode;
    Node* unitsNode;
    Node* smUnitsNode;
    Node* terrainNode;
};