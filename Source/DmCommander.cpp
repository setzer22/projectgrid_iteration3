#include "DmCommander.h"
#include <iostream>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Container/Vector.h>
#include "UnitStats.h"
#include "Debug.h"
#include "DmStateInfo.h"

using namespace std;

DmCommander::DmCommander(Context* context) :
    LogicComponent(context) {

    lastTile = IntVector2(-2,-2);
}

void DmCommander::RegisterObject(Context* context) {
    context->RegisterFactory<DmCommander>("DungeonMaster");
}


void DmCommander::Start() {

}

void DmCommander::Update() {

}

IntVector2 DmCommander::GetMouseTile() {
    IntVector2 screenPos = GetSubsystem<Input>()->
                           GetMousePosition();
    float width = GetSubsystem<Graphics>()->GetWidth();
    float height = GetSubsystem<Graphics>()->GetHeight();
    return mouseComponent->WorldToMap(
               Vector2((float)screenPos.x_/width,
                       (float)screenPos.y_/height));

}

bool DmCommander::QueryMouseTile(IntVector2& tile) {
    tile = GetMouseTile();
    bool b = tile.x_ != -1 &&
             (tile.x_ != lastTile.x_ || tile.y_ != lastTile.y_);
    if(b) lastTile = tile;
    return b;
}

void DmCommander::PaintInRange(PathfinderInfo* pathInfo, Color& color,
                               bool attackRange) {
    
    Vector<IntVector2>* range = (attackRange)?pathInfo->AttackRange:
                                pathInfo->MoveRange;
    for(int i = 0; i < range->Size(); ++i) {
        cout << (*range)[i].x_ << " " << (*range)[i].y_ << endl;
        graphicsComponent->PaintTile(color,
                                     (*range)[i].x_, (*range)[i].y_);
    }
}

void DmCommander::SetTileGUI(IntVector2 tile) {
    guiComponent->SetTile(tile, dataComponent->TileName(tile),
                          dataComponent->TileCost(tile));
}


void DmCommander::SetUnitGUI(Node* unit) {
    UnitStats* stats = unit->GetComponent<UnitStats>();
    guiComponent->SetUnit(stats->unitName, stats->HP, stats->moveRange, stats->attackRange);
}

Variant DmCommander::GetUnitsInRange(PathfinderInfo* pathInfo, 
  bool attackRange, bool excludeOrigin) {
    
    Vector<IntVector2>* range = (attackRange)?pathInfo->AttackRange:
                                              pathInfo->MoveRange;
    Vector<Variant> v;
    for(int i = 0; i < range->Size(); ++i) {
        IntVector2 t = (*range)[i];
        if(excludeOrigin and t.x_ == 0 and t.y_ == 0) continue;
        Node* unit = dataComponent->TileUnit(t.x_,t.y_);        
        if(unit) v.Push(Variant(unit));
    }
    return Variant(v);    
}

Variant DmCommander::GetUnitsInTeam(DmStateInfo* state, 
  Team team, int minActionPoints) {
    
    Vector<Variant> units_var;
    units_var.Reserve(state->Teams[team].Size());
    for(int i = 0; i < state->Teams[team].Size(); ++i) {
        Node* unit = state->Teams[team][i]; 
        if(unit->GetComponent<UnitStats>()->actionPoints >= minActionPoints)
            units_var.Push(Variant(unit));
    }
    return Variant(units_var);
}


void DmCommander::ApplyAttributes() {

    SetEnabled(true);

    Node* node = GetScene()->GetChild("Map");
    TESTNULL(node, "DM", "Commander", "The Map node is not in the scene");

    mouseComponent = node->GetComponent<MapMouseToTile>();
    TESTNULL(mouseComponent, "DM", "Commander", "The map node doesn't appear to have a mouse component");

    graphicsComponent = node->GetComponent<MapGraphics>();
    TESTNULL(graphicsComponent, "DM", "Commander", "The map node doesn't appear to have a graphics component");

    pathfinderComponent = node->GetComponent<MapPathfinder>();
    TESTNULL(pathfinderComponent, "DM", "Commander", "The map node doesn't appear to have a pathfinder component");

    dataComponent = node->GetComponent<MapData>();
    TESTNULL(dataComponent, "DM", "Commander", "The map node doesn't appear to have a data component");
    
    detailComponent = node->GetComponent<MapDetail>();
    TESTNULL(detailComponent, "DM", "Commander", "The map node doesn't appear to have a detail component");

    Node* camNode = GetScene()->GetChild("Camera");
    TESTNULL(camNode, "DM", "Commander", "The Camera node is not in the scene");

    cameraComponent = camNode->GetComponent<CameraMovement>();
    TESTNULL(dataComponent, "DM", "Commander", "The camera node doesn't appear to have a movement component");

    Node* guiNode = GetScene()->GetChild("GUI");
    TESTNULL(guiNode, "DM", "Commander", "There's no GUI node in the scene");

    guiComponent = guiNode->GetComponent<GUIMain>();
    TESTNULL(guiComponent, "DM", "Commander", "There's no GUI component");

}

void DmCommander::BindScriptMethods(asIScriptEngine *engine) {
    engine->RegisterObjectType("DmCommander",0, asOBJ_REF);
    engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "DmCommander@ f()",
                                    asFUNCTION((RefCast<Component,DmCommander>)), asCALL_CDECL_OBJLAST);
    engine->RegisterObjectBehaviour("DmCommander", asBEHAVE_ADDREF, "void f()", asMETHOD(DmCommander,AddRef), asCALL_THISCALL);
    engine->RegisterObjectBehaviour("DmCommander", asBEHAVE_RELEASE, "void f()", asMETHOD(DmCommander,ReleaseRef), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "MapMouseToTile@ GetMouse()", asMETHOD(DmCommander, GetMouse), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "MapGraphics@ GetGraphics()", asMETHOD(DmCommander, GetGraphics), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "MapData@ GetData()", asMETHOD(DmCommander, GetData), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "MapDetail@ GetDetail()", asMETHOD(DmCommander, GetDetail), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "CameraMovement@ GetCamera()", asMETHOD(DmCommander, GetCamera), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "MapPathfinder@ GetPathfinder()", asMETHOD(DmCommander, GetPathfinder), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "IntVector2 GetMouseTile()", asMETHOD(DmCommander, GetMouseTile), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "bool QueryMouseTile(IntVector2 &out tile)", asMETHOD(DmCommander, QueryMouseTile), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "void PaintInRange(PathfinderInfo@ pathInfo, Color &in color, bool attackRange = false)", asMETHOD(DmCommander, PaintInRange), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "GUIMain@ GetGUI()", asMETHOD(DmCommander, GetGUI), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "void SetUnitGUI(Node@ unit)", asMETHOD(DmCommander, SetUnitGUI), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "void SetTileGUI(IntVector2 tile)", asMETHOD(DmCommander, SetTileGUI), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "Variant GetUnitsInRange(PathfinderInfo@ pathInfo, bool attackRange = false, bool ignoreOrigin = false)", asMETHOD(DmCommander, GetUnitsInRange), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "Variant GetUnitsInTeam(DmStateInfo@ state, Team team, int minActionPoints = -1)", asMETHOD(DmCommander, GetUnitsInTeam), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "Team intToTeam(int t)", asMETHOD(DmCommander, intToTeam), asCALL_THISCALL);
    engine->RegisterObjectMethod("DmCommander", "int teamToInt(Team t)", asMETHOD(DmCommander, teamToInt), asCALL_THISCALL);
}
