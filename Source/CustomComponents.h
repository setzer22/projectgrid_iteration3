#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Script/APITemplates.h>
#include "MapGraphics.h"

class CustomComponents {
public:
	static void RegisterObjects(Urho3D::Context* context);

	static void BindScriptMethods(asIScriptEngine* engine);

private:
};
