#include "GUIMain.h"
#include "Debug.h"

#include <Urho3D/UI/UI.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/UIEvents.h>

GUIMain::GUIMain(Context* context) : LogicComponent(context) {
    this->context = context;
}

void GUIMain::RegisterObject(Context* context) {
    context->RegisterFactory<GUIMain>("GUI");
    ATTRIBUTE("Unit Window", bool, unitVisible, false, AM_DEFAULT);
    ATTRIBUTE("Tile Window", bool, tileVisible, false, AM_DEFAULT);
    ATTRIBUTE("Action Window", bool, actionVisible, false, AM_DEFAULT);
}

void GUIMain::ApplyAttributes() {
        SetActionVisible(actionVisible);
        SetUnitVisible(unitVisible);
        SetTileVisible(tileVisible);
}

void GUIMain::Start() {
    UI* ui = GetSubsystem<UI>(); 
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    
    style = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");
    ui->GetRoot()->SetDefaultStyle(style);
    
    //Unit Window
    tileWindow = new Window(context);
    ui->GetRoot()->AddChild(tileWindow);
    tileWindow->SetMinSize(384, 192); 
    tileWindow->SetLayout(Urho3D::LM_VERTICAL, 6, IntRect(6, 6, 6, 6));
    tileWindow->SetAlignment(Urho3D::HA_CENTER, Urho3D::VA_CENTER);
    tileWindow->SetName("TileWindow");
    tileWindow->SetPosition(400,-300); //TODO: Runtime position editing
    
    initText(tileInfo, "TileInfo", "Tile Information: "); tileWindow->AddChild(tileInfo);
    initText(tileCoords, "TileCoords", "Coordinates: "); tileWindow->AddChild(tileCoords);
    initText(tileName, "TileName", "Name: "); tileWindow->AddChild(tileName);
    initText(tileCost, "TileCost", "Movement Cost: "); tileWindow->AddChild(tileCost);
    
    tileWindow->SetStyleAuto();
    
    unitWindow = new Window(context);
    ui->GetRoot()->AddChild(unitWindow);
    unitWindow->SetMinSize(384, 192); 
    unitWindow->SetLayout(Urho3D::LM_VERTICAL, 6, IntRect(6, 6, 6, 6));
    unitWindow->SetAlignment(Urho3D::HA_CENTER, Urho3D::VA_CENTER);
    unitWindow->SetName("UnitWindow");
    unitWindow->SetPosition(-300,-300); //TODO: Runtime position editing
    
    initText(unitName, "UnitName", "Name: "); unitWindow->AddChild(unitName);
    initText(unitHP, "UnitHP", "HP: "); unitWindow->AddChild(unitHP);
    initText(unitRng, "UnitRange", "Movement Range: "); unitWindow->AddChild(unitRng);
    initText(unitAtkRng, "UnitAttackRange", "Attack Range: "); unitWindow->AddChild(unitAtkRng);
    
    unitWindow->SetStyleAuto();
    
    actionWindow = StaticCast<Window>(ui->LoadLayout(cache->GetResource<XMLFile>("UI/ActionsWindow.xml")));
    actionWindow->SetStyleAuto();
    ui->GetRoot()->AddChild(actionWindow);
    
    SubscribeToEvent(actionWindow->GetChild("AttackButton",false), Urho3D::E_RELEASED, 
                     HANDLER(GUIMain,AttackPressed));
    SubscribeToEvent(actionWindow->GetChild("MoveButton",false), Urho3D::E_RELEASED, 
                     HANDLER(GUIMain,AttackPressed));
    SubscribeToEvent(actionWindow->GetChild("StayButton",false), Urho3D::E_RELEASED, 
                     HANDLER(GUIMain,AttackPressed));
    SubscribeToEvent(actionWindow->GetChild("UselessButton1",false), Urho3D::E_RELEASED, 
                     HANDLER(GUIMain,AttackPressed));
    
}

void GUIMain::SetTile(IntVector2 coords, String name, float cost) {
    UI* ui = GetSubsystem<UI>(); 
    static_cast<Text*>(tileWindow->GetChild("TileName", false))->
        SetText(String("Name: ")+String(name));
    static_cast<Text*>(tileWindow->GetChild("TileCost", false))->
        SetText(String("Movement Cost: ")+String(cost));
    static_cast<Text*>(tileWindow->GetChild("TileCoords", false))->
        SetText(String("Coordinates: (")+String(coords.x_)+String(",")+String(coords.y_)+String(")"));
}

void GUIMain::SetUnit(String name, int HP, int moveRange, int atkRange) {
    UI* ui = GetSubsystem<UI>(); 
    static_cast<Text*>(unitWindow->GetChild("UnitName", false))->
        SetText(String("Name: ")+String(name));
    static_cast<Text*>(unitWindow->GetChild("UnitHP", false))->
        SetText(String("HP: ")+String(HP));
    static_cast<Text*>(unitWindow->GetChild("UnitRange", false))->
        SetText(String("Movement Range: ")+String(moveRange));
    static_cast<Text*>(unitWindow->GetChild("UnitAttackRange", false))->
        SetText(String("Attack Range: ")+String(atkRange));
}

void GUIMain::initText(SharedPtr<Text>& text, String name, String l_text) {
    text = new Text(context);
    Font* font = GetSubsystem<ResourceCache>()->GetResource<Font>(FONT_NAME);
    text->SetText(l_text);
    text->SetName(name);
    text->SetFont(font, 15);
}


void GUIMain::SetActionVisible(bool visible) {
    actionWindow->SetVisible(visible);
    auto v = actionWindow->GetChildren();
    for(int i = 0; i < v.Size(); ++i) {
        v[i]->SetVisible(visible);
    }
}

void GUIMain::SetTileVisible(bool visible) {
    tileWindow->SetVisible(visible);
}

void GUIMain::SetUnitVisible(bool visible) {
    unitWindow->SetVisible(visible);
}

void GUIMain::AttackPressed(StringHash eventType, VariantMap& eventData) {
    UIElement* button = static_cast<UIElement*>
        (eventData["Element"].GetPtr());
    VariantMap eventData2;
    eventData2["Button"] = Variant(button->GetName());
    SendEvent("ActionPress",eventData2);
}


void GUIMain::BindScriptMethods(asIScriptEngine *engine) {
    int r;
    r = engine->RegisterObjectType("GUIMain",0, asOBJ_REF); assert (r>=0);
    r = engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "GUIMain@ f()",
                                    asFUNCTION((RefCast<Component,GUIMain>)), asCALL_CDECL_OBJLAST); assert (r>=0);
    r = engine->RegisterObjectBehaviour("GUIMain", asBEHAVE_ADDREF, "void f()", asMETHOD(GUIMain,AddRef), asCALL_THISCALL); assert (r>=0);
    r = engine->RegisterObjectBehaviour("GUIMain", asBEHAVE_RELEASE, "void f()", asMETHOD(GUIMain,ReleaseRef), asCALL_THISCALL); assert (r>=0);    
    r = engine->RegisterObjectMethod("GUIMain", "void SetUnitVisible(bool visible)", asMETHOD(GUIMain, SetUnitVisible), asCALL_THISCALL); assert (r>=0);
    r = engine->RegisterObjectMethod("GUIMain", "void SetActionVisible(bool visible)", asMETHOD(GUIMain, SetActionVisible), asCALL_THISCALL); assert (r>=0);
    r = engine->RegisterObjectMethod("GUIMain", "void SetTileVisible(bool visible)", asMETHOD(GUIMain, SetTileVisible), asCALL_THISCALL); assert (r>=0);
}