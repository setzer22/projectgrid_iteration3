#include "StateManager.h"
#include <Urho3D/Container/Ptr.h>
#include <iostream>

using namespace std;

StateManager::StateManager(ResourceCache* thecache, 
  FileSystem* fs) {
	cache = thecache;
	fileSystem = fs;
}

StateManager::~StateManager() {
}

void StateManager::Init() {

	String path = fileSystem->GetProgramDir()+
		String("Data/Scripts/GameStates");

	Vector<String> ls;

	fileSystem->ScanDir(ls, path, String("as"), Urho3D::SCAN_FILES, false);

	for(int i = 0; i < ls.Size(); ++i) {
		cout << ls[i].CString() << endl;
		GameState* state = cache->GetResource<ScriptFile>
            (String(GAME_STATES_PATH)+String("/")+ls[i]);
        ls[i].Resize(ls[i].Length()-3);
        cout << ls[i].CString() << endl;
        stateLibrary[ls[i].CString()] = SharedPtr<GameState>(state);
	}
	//...
}

GameState* StateManager::getState(string name) {
	return stateLibrary[name].Get();
}
