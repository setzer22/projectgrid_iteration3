#include "MapProperties.h"
#include <Urho3D/Resource/ResourceCache.h>
#include <iostream>
#include <string>

using namespace std;

MapProperties::MapProperties(Context* context) : LogicComponent(context) {

}

void MapProperties::RegisterObject(Context* context) {
    context->RegisterFactory<MapProperties>("Map");
    //ACCESSOR_ATTRIBUTE("Map data", MapProperties::GetXmlAttr, MapProperties::SetXmlAttr, ResourceRef, ResourceRef(XMLFile::GetTypeStatic()), AM_DEFAULT);
}

void MapProperties::ApplyAttributes() {
    if(mapXml != NULL) {
        XMLElement mapNode = mapXml->GetRoot("map");
        if(mapNode.IsNull()) {
            cerr << "[Map]:[Properties] ERROR: Wrong xml map format.";
            return;
        }
        height = stoi(mapNode.GetChild("rows").GetValue().CString());
        width = stoi(mapNode.GetChild("columns").GetValue().CString());
    }
    else {
        cerr << "[Map]:[Preperties] ERROR: The xml map is not set." << endl;
        //@hack - While I can't manage to make this work I'll hardcode the map XML in here.
        mapXml = GetSubsystem<ResourceCache>()->GetResource<XMLFile>("Maps/Xml/map_demo_2.xml");
        XMLElement mapNode = mapXml->GetRoot("map");
        if(mapNode.IsNull()) {
            cerr << "[Map]:[Properties] ERROR: Wrong xml map format.";
            return;
        }
        height = stoi(mapNode.GetChild("rows").GetValue().CString());
        width = stoi(mapNode.GetChild("columns").GetValue().CString());    
        cout << "Width = "<<width<<" Height= "<<height<<endl;
        // @/hack
        return;
    }
}

void MapProperties::Start() {
    cout << "[Map]:[Properties] INFO: Starting properties component." << endl;
}

int MapProperties::Width() {
    return width;
}

int MapProperties::Height() {
    return height;
}

XMLFile* MapProperties::XmlData() {
    return mapXml;
}

void MapProperties::SetXmlAttr(const ResourceRef value) {
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    mapXml = cache->GetResource<XMLFile>(value.name_);
}

ResourceRef MapProperties::GetXmlAttr() const {
    return GetResourceRef(mapXml, XMLFile::GetTypeStatic());
}

void MapProperties::BindScriptMethods(asIScriptEngine *engine) {
    engine->RegisterObjectType("MapProperties",0, asOBJ_REF);
    engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "MapProperties@ f()", 
        asFUNCTION((RefCast<Component,MapProperties>)), asCALL_CDECL_OBJLAST);
    engine->RegisterObjectBehaviour("MapProperties", asBEHAVE_ADDREF, "void f()", asMETHOD(MapProperties,AddRef), asCALL_THISCALL);
    engine->RegisterObjectBehaviour("MapProperties", asBEHAVE_RELEASE, "void f()", asMETHOD(MapProperties,ReleaseRef), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapProperties", "int Width()", asMETHOD(MapProperties, Width), asCALL_THISCALL);
    engine->RegisterObjectMethod("MapProperties", "int Height()", asMETHOD(MapProperties, Height), asCALL_THISCALL);
}
