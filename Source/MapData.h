#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>
#include <Urho3D/Container/Ptr.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Container/Vector.h>
#include "GlobalEnums.h"
#include <Urho3D/Core/Context.h>
#include "MapProperties.h"

using namespace std;
using namespace Urho3D;

struct Tile {
	
public:

	String name;
	float cost;
	bool walkable;
	SharedPtr<Node> unit;

};

class MapData : public LogicComponent { 
	OBJECT(MapData)  

public:

	void AddRef() { /* do nothing */ }
    void ReleaseRef() { /* do nothing */ }

	MapData(Context* context);

	static void RegisterObject(Context* context);

    virtual void ApplyAttributes();

    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();

    bool TileWalkable (int x, int y);

    bool TileWalkable (IntVector2 p);

    bool TileWalkable (IntVector2 p, MoveType moveType, Team team);

    String TileName (int x, int y);        String TileName (IntVector2 p);

    float TileCost (int x, int y);        float TileCost (IntVector2 p);

    bool TileOccupied (int x, int y);    bool TileOccupied (IntVector2 p);

    Node* TileUnit (int x, int y);         Node* TileUnit (IntVector2 p);

    void SetUnit (Node* unit, int x, int y); void SetUnit (Node* unit, IntVector2 p);
  
private:
	
	SharedPtr<XMLFile> mapFile;
	Vector<Vector<Tile>> tileMap;
	MapProperties* propertiesComponent;

	bool IsTeamCompatible(Team a, Team b); //Shouldn't go here, but I don't
										   // think I'll need anything more complex

	void ParseMap ();

	void CheckSafe(int x, int y);

	Vector<int> ParseMapData(String& str);


};