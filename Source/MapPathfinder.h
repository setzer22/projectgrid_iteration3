#pragma once

#include <Urho3D/Urho3D.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Script/APITemplates.h>
#include "GlobalEnums.h"
#include "PathfinderInfo.h"
#include "MapData.h"
#include "MapProperties.h"

using namespace Urho3D;
using namespace std;

class MapPathfinder : public LogicComponent { 
	OBJECT(MapPathfinder)  

public:

	void AddRef() { /* do nothing */ }
    void ReleaseRef() { /* do nothing */ }

	MapPathfinder(Context* context);

	static void RegisterObject(Context* context);

    virtual void ApplyAttributes();

    static void BindScriptMethods(asIScriptEngine *engine);
    
    virtual void Start();

    PathfinderInfo* GetPathInfo(IntVector2 ini, float moveRange, int attackRange,
    	int minAttackRange, MoveType moveType, Team team);
    
    PathfinderInfo* GetLastPathInfo();

    Vector<IntVector2> GetShortestPath(PathfinderInfo& pathInfo, IntVector2 dest);

    CScriptArray* GetShortestPath_script(PathfinderInfo* pathInfo, IntVector2 dest);
  
private:

	int mapW, mapH;

	WeakPtr<MapData> dataComponent;
	WeakPtr<MapProperties> propertiesComponent;
        
    SharedPtr<PathfinderInfo> lastPathInfo;

	//CScriptArray* path_script_array;
		
	bool InBounds(int x, int y);

	Vector<IntVector2> GetNeighbours(IntVector2 p, MoveType moveType, Team team);

	void CheckAllInRange(int attackRange, int minAttackRange, IntVector2 ini, 
		Vector<IntVector2>& inMoveRange, Vector<IntVector2>& inAttackRange, Team team);

	float GetDistMap(IntVector2 p, vector<vector<float>>& distMap);

	void SetDistMap(IntVector2 p, float value, vector<vector<float>>& distMap);

};

class NodeComparator
{
public:
	NodeComparator (vector<vector<float>>* distMap) {
		this->distMap = distMap;
	}

    bool operator() (IntVector2 p1, IntVector2 p2)
    {
        return (*distMap)[distMap->size() - p1.y_ - 1][p1.x_] < (*distMap)[distMap->size() - p2.y_ - 1][p2.x_];
    }
private:
	vector<vector<float>>* distMap;
};