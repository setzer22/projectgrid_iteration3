#include "MapPathfinder.h"
#include <queue>
#include <vector>
#include <limits>
#include <iostream>

MapPathfinder::MapPathfinder(Context* context) : LogicComponent(context) {
	//path_script_array = NULL;	
} 

void MapPathfinder::RegisterObject(Context* context) {
	context->RegisterFactory<MapPathfinder>("Map");
}

void MapPathfinder::ApplyAttributes() {

}

void MapPathfinder::Start() {
	propertiesComponent = (MapProperties*)node_->GetComponent("MapProperties");
	dataComponent = (MapData*)node_->GetComponent("MapData");
	if(propertiesComponent == NULL) {
		cerr << "[Map][Pathfinder] ERROR: Properties component not present in the node. Cannot start.";
		return;
	}
	if(dataComponent == NULL) {
		cerr << "[Map][Properties] ERROR: Data component not present in the node. Cannot start.";
	}
	uint s = 0;
	//path_script_array = CScriptArray::Create(GetSubsystem<Script>()->GetObjectType("IntVector2"), s);
}

PathfinderInfo* MapPathfinder::GetPathInfo(IntVector2 ini, float moveRange, int attackRange,
	int minAttackRange, MoveType moveType, Team team) {

	//Initialisation
	Vector<IntVector2> inRange;
	vector<vector<float>> distMap (propertiesComponent->Height(), 
		vector<float>(propertiesComponent->Width(),std::numeric_limits<float>::infinity()));
	
	//creating a priority queue
	priority_queue<IntVector2, std::vector<IntVector2>, NodeComparator> pq((NodeComparator(&distMap)));
	
	//Move range:
	pq.push(ini);
	inRange.Push(ini);

	SetDistMap(ini, 0, distMap);
	
	while(!pq.empty()) {
		IntVector2 u = pq.top(); pq.pop();
		Vector<IntVector2> neighbours = GetNeighbours(u, ATTACK, team);
		for (int i = 0; i < neighbours.Size(); ++i) {IntVector2 v = neighbours[i];
			if(GetDistMap(v,distMap) > dataComponent->TileCost(v)+GetDistMap(u,distMap)) {
				SetDistMap(v,dataComponent->TileCost(v)+GetDistMap(u,distMap),distMap);
				
				if(GetDistMap(v,distMap) <= moveRange) {
					inRange.Push(v);
					if(GetDistMap(v,distMap) < moveRange) {
						
						pq.push(v);
					}
				}
			}
		}
	}

	

	//Attack range:
	Vector<IntVector2> inAttackRange;
	if(attackRange > 0) {
		for(int i = 0; i < inRange.Size(); ++i) { IntVector2 p = inRange[i];
			CheckAllInRange(attackRange, minAttackRange, p, inRange, inAttackRange, team);
		}
	}
	PathfinderInfo* p = new PathfinderInfo(inRange, inAttackRange, ini, distMap, moveType, team);
    lastPathInfo = p;
	return p;
}

PathfinderInfo* MapPathfinder::GetLastPathInfo() {
    return lastPathInfo;
}

bool MapPathfinder::InBounds(int x, int y) {
	return (0 <= x && x < propertiesComponent->Width()) && (0 <= y && y < propertiesComponent->Height());
}	

Vector<IntVector2> MapPathfinder::GetNeighbours(IntVector2 p, MoveType moveType, Team team) {
	Vector<IntVector2> neighbours(0);

	IntVector2 directions[4] = {IntVector2(1,0), IntVector2(-1,0), IntVector2(0,1), IntVector2(0,-1)};
	for(int i = 0; i < 4; ++i) {
		IntVector2 v = p + directions[i];
		if(InBounds(v.x_,v.y_) && dataComponent->TileWalkable(v,moveType,team)) 
			neighbours.Push(v);
	}
	return neighbours;
}

void MapPathfinder::CheckAllInRange(int attackRange, int minAttackRange, IntVector2 ini, 
  Vector<IntVector2>& inMoveRange, Vector<IntVector2>& inAttackRange, Team team) {
	
	queue<IntVector2> q;
	q.push(ini);
	vector<vector<float>> distMap (propertiesComponent->Height(), 
		vector<float>(propertiesComponent->Width(),std::numeric_limits<float>::infinity()));
	
	SetDistMap (ini, 0, distMap);

	while (q.size() != 0) {
		IntVector2 vdq = q.front(); q.pop();
		Vector<IntVector2> neighbours = GetNeighbours(vdq, /*MoveType*/ATTACK, team);
		for (int i = 0; i < neighbours.Size(); ++i)	{ IntVector2 n = neighbours[i];
			float d = GetDistMap(vdq, distMap) + 1;
			SetDistMap(n, d, distMap);
			if(d >= minAttackRange && d <= attackRange) inAttackRange.Push(n);
			if(d < attackRange) q.push(n);
		}
	}
}

#define inf_ std::numeric_limits<float>::infinity()
#define distMap_(i,j) GetDistMap(IntVector2(i,j),(*(pathInfo.DistMap)))

Vector<IntVector2> MapPathfinder::GetShortestPath(PathfinderInfo& pathInfo, IntVector2 dest) {
	Vector<IntVector2> result(0);
	if(distMap_(dest.x_,dest.y_) == inf_) {
		result.Push(IntVector2(-1,-1));
		return result;
	}
	IntVector2 current = dest;
	result.Push(current);

	int dirs[8] = {0,1,0,-1,-1,0,1,0};
	IntVector2 s = pathInfo.source;
	while(current != s) {
		float minDist = inf_;
		int minIndex = -1;
		for(int i = 0; i < 8; i += 2) {
			float dist = distMap_(current.x_+dirs[i],current.y_+dirs[i+1]);
			if(dist != -1 && dist < minDist) {
				minDist = dist;
				minIndex = i;
			}
		}
		IntVector2 next = current+IntVector2(dirs[minIndex],dirs[minIndex+1]);
		result.Push(next);
		current = next;
	}
	return result;
}

CScriptArray* MapPathfinder::GetShortestPath_script(PathfinderInfo* pathInfo, IntVector2 dest) {
	Vector<IntVector2> path = GetShortestPath(*pathInfo, dest);
	return VectorToArray<IntVector2>(path, "Array<IntVector2>");
}


float MapPathfinder::GetDistMap(IntVector2 p, vector<vector<float>>& distMap) {
	if(p.x_ < 0 || p.x_ >= distMap[0].size() || p.y_ < 0 || p.y_ >= distMap.size()) return -1;
	else return distMap[distMap.size() - p.y_ - 1][p.x_];
}

void MapPathfinder::SetDistMap(IntVector2 p, float value, vector<vector<float>>& distMap) {
	if(p.x_ < 0 || p.x_ >= distMap[0].size() || p.y_ < 0 || p.y_ >= distMap.size()) return;
	else distMap[distMap.size() - p.y_ - 1][p.x_] = value;
}

void MapPathfinder::BindScriptMethods(asIScriptEngine *engine) {
	engine->RegisterObjectType("MapPathfinder",0, asOBJ_REF);
	engine->RegisterObjectBehaviour("Component", asBEHAVE_IMPLICIT_REF_CAST, "MapPathfinder@ f()", 
		asFUNCTION((RefCast<Component,MapPathfinder>)), asCALL_CDECL_OBJLAST);
	engine->RegisterObjectBehaviour("MapPathfinder", asBEHAVE_ADDREF, "void f()", asMETHOD(MapPathfinder,AddRef), asCALL_THISCALL);
	engine->RegisterObjectBehaviour("MapPathfinder", asBEHAVE_RELEASE, "void f()", asMETHOD(MapPathfinder,ReleaseRef), asCALL_THISCALL);		
	engine->RegisterObjectMethod("MapPathfinder", "PathfinderInfo@+ GetPathInfo(IntVector2 ini, float moveRange, int attackRange, int minAttackRange, MoveType moveType, Team team)", asMETHOD(MapPathfinder, GetPathInfo), asCALL_THISCALL);
	engine->RegisterObjectMethod("MapPathfinder", "Array<IntVector2>@ GetShortestPath(PathfinderInfo@+ pathInfo, IntVector2 dest)", asMETHOD(MapPathfinder,GetShortestPath_script),asCALL_THISCALL);
    engine->RegisterObjectMethod("MapPathfinder", "PathfinderInfo@+ GetLastPathInfo()", asMETHOD(MapPathfinder, GetLastPathInfo), asCALL_THISCALL);
}
