#include "Uniforms.glsl"
#include "Samplers.glsl"
#include "Transform.glsl"
#include "ScreenPos.glsl"
#include "Lighting.glsl"
#include "Fog.glsl"

varying vec2 vTexCoord;
varying vec2 vDetailTexCoord;
varying vec3 vNormal;
varying vec4 vWorldPos;
#ifdef PERPIXEL
    #ifdef SHADOW
        varying vec4 vShadowPos[NUMCASCADES];
    #endif
    #ifdef SPOTLIGHT
        varying vec4 vSpotPos;
    #endif
    #ifdef POINTLIGHT
        varying vec3 vCubeMaskVec;
    #endif
#else
    varying vec3 vVertexLight;
    varying vec4 vScreenPos;
    #ifdef ENVCUBEMAP
        varying vec3 vReflectionVec;
    #endif
    #if defined(LIGHTMAP) || defined(AO)
        varying vec2 vTexCoord2;
    #endif
#endif

uniform sampler2D sWeightMap0;
uniform sampler2D sDetailMap1;
uniform sampler2D sDetailMap2;
uniform sampler2D sDetailMap3;
uniform sampler2D sTileMap4;

uniform vec2 cDetailTiling;
uniform vec2 cgrid_Dimensions;
uniform vec2 cgrid_Origin;
uniform vec2 cgrid_TexSize;
uniform float cgrid_SquareSize;
uniform float cgrid_GridWidth;
uniform float cgrid_ColorMultiplier;
uniform float cgrid_RoundCorner;

void VS()
{
    mat4 modelMatrix = iModelMatrix;
    vec3 worldPos = GetWorldPos(modelMatrix);
    gl_Position = GetClipPos(worldPos);
    vNormal = GetWorldNormal(modelMatrix);
    vWorldPos = vec4(worldPos, GetDepth(gl_Position));
    vTexCoord = GetTexCoord(iTexCoord);
    vDetailTexCoord = cDetailTiling * vTexCoord;

    #ifdef PERPIXEL
        // Per-pixel forward lighting
        vec4 projWorldPos = vec4(worldPos, 1.0);

        #ifdef SHADOW
            // Shadow projection: transform from world space to shadow space
            for (int i = 0; i < NUMCASCADES; i++)
                vShadowPos[i] = GetShadowPos(i, projWorldPos);
        #endif

        #ifdef SPOTLIGHT
            // Spotlight projection: transform from world space to projector texture coordinates
            vSpotPos = cLightMatrices[0] * projWorldPos;
        #endif
    
        #ifdef POINTLIGHT
            vCubeMaskVec = mat3(cLightMatrices[0][0].xyz, cLightMatrices[0][1].xyz, cLightMatrices[0][2].xyz) * (worldPos - cLightPos.xyz);
        #endif
    #else
        // Ambient & per-vertex lighting
        #if defined(LIGHTMAP) || defined(AO)
            // If using lightmap, disregard zone ambient light
            // If using AO, calculate ambient in the PS
            vVertexLight = vec3(0.0, 0.0, 0.0);
            vTexCoord2 = iTexCoord2;
        #else
            vVertexLight = GetAmbient(GetZonePos(worldPos));
        #endif
        
        #ifdef NUMVERTEXLIGHTS
            for (int i = 0; i < NUMVERTEXLIGHTS; ++i)
                vVertexLight += GetVertexLight(i, worldPos, vNormal) * cVertexLights[i * 3].rgb;
        #endif
        
        vScreenPos = GetScreenPos(gl_Position);

        #ifdef ENVCUBEMAP
            vReflectionVec = worldPos - cCameraPos;
        #endif
    #endif
}

void PS()
{
	float tx = floor((vWorldPos.x-cgrid_Origin.x) / cgrid_SquareSize);
	float ty = floor((vWorldPos.z-cgrid_Origin.y) / cgrid_SquareSize);
    float cx = abs(mod(vWorldPos.x-cgrid_Origin.x,cgrid_SquareSize));
    float cz = abs(mod(vWorldPos.z-cgrid_Origin.y,cgrid_SquareSize));
    if(0.0 <= tx && tx < cgrid_Dimensions.x && 0.0 <= ty && ty < cgrid_Dimensions.y) {
        vec4 tile = texture2D(sTileMap4, vec2(tx / cgrid_TexSize.x, 1.0-( (ty+1.0) / cgrid_TexSize.y) ));
        if(tile.rgb != vec3(1.0, 1.0, 1.0)) {
			if(!((cx > cgrid_SquareSize-cgrid_GridWidth && cx < cgrid_SquareSize+cgrid_GridWidth) ||
			 (cz > cgrid_SquareSize-cgrid_GridWidth && cz < cgrid_SquareSize+cgrid_GridWidth))) {
			 
			 	float dx = (cx+cgrid_GridWidth/2.0-cgrid_SquareSize/2.0);
			 	float dy = (cz+cgrid_GridWidth/2.0-cgrid_SquareSize/2.0);
			 
				float dist = cgrid_ColorMultiplier*2.0*max(abs(dx),abs(dy)) / cgrid_SquareSize;
				float rdist = 2.0*sqrt(dx*dx + dy*dy) / cgrid_SquareSize;	
				if(rdist > cgrid_RoundCorner) dist = 0.0;
				
				gl_FragColor = vec4(tile.r, tile.g, tile.b, dist);
			}
			else {
				gl_FragColor = vec4(1.0, 1.0, 1.0, 0.0);		
			}
		}
		else {
			gl_FragColor = vec4(1.0, 1.0, 1.0, 0.0);		
		}
	}
	else 
		gl_FragColor = vec4(1.0, 1.0, 1.0, 0.0);
		
    
}
