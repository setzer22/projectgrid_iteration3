class PlaceUnits : ScriptObject
{
	int unitsNodeId;
	int mapNodeId;
	String unitLocations;
	String output;

	void Start() {
		
	}

	void ApplyAttributes() {
		Print("Applying Attributes");
		String[] positions = unitLocations.Split(';');

		MapData@ mapData = cast<MapData>(scene.GetNode(mapNodeId).
			GetComponent("MapData"));

		Node@[] units = scene.GetNode(unitsNodeId).GetChildren();
		for(int i = 0; i < units.length; ++i) {
			String[] position = positions[i].Split(',');
			mapData.SetUnit(units[i], position[0].ToInt(), 
				position[1].ToInt());
            Node@ smNode = scene.GetNode(units[i].vars["SmUnitId"].GetInt());
            smNode.position = units[i].position;
            smNode.rotation = units[i].rotation;

		}
	}

	void Update(float timeStep) {
	}

}
