class CameraTest : ScriptObject {

	float time = 0;
	bool watchMode = false;
	int unitNodeId;

	void Start() {

	}

	void ApplyAttributes() {
		CameraMovement@ camera = cast<CameraMovement@>(node.GetComponent("CameraMovement"));
		Node@ unit = scene.GetNode(unitNodeId);	
		if(unit != null) camera.SetUnitNode(unit);
	}

	void Update(float timeStep) {
		CameraMovement@ camera = cast<CameraMovement@>(node.GetComponent("CameraMovement"));
		time += timeStep;
		if(time >= 2) {
			camera.SetMode(watchMode);
			Print("aaa");
			watchMode = !watchMode;
			time = 0;
		}
	}

}