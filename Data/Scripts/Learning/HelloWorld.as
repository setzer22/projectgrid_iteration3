// This first example, maintaining tradition, prints a "Hello World" message.
// Furthermore it shows:
//     - Using the Sample utility functions as a base for the application
//     - Adding a Text element to the graphical user interface
//     - Subscribing to and handling of update events

#include "Scripts/Utilities/Sample.as"

void Start()
{

    SampleStart();

    LoadScene();

    SetupViewport();

    SubscribeToEvents();

}

Scene@ gameScene;
Node@ cameraNodeBis;
Texture2D@ texture;
Image@ image;

void LoadScene() {
	gameScene = Scene("TestScene");

	script.defaultScene = gameScene;

	gameScene.LoadXML(cache.GetFile("Scenes/TerrainExperiment2.xml"));

	image = cache.GetResource("Image", "Textures/TileMap1.png");
	image.SetPixel(0,0,Color(1.0,0.0,0.0,1.0));

	cameraNodeBis = gameScene.GetNode(5); 
	Node@ terrainNode = gameScene.GetNode(9);

	Terrain@ terrain = terrainNode.GetComponent("Terrain");
	Material@ material = terrain.material;
	texture = cast<Texture2D>(material.textures[4]);
	texture.SetData(image, false);




}

void SubscribeToEvents() {
    // Subscribe HandleUpdate() function for processing update events
    SubscribeToEvent("Update", "HandleUpdate");
}

void SetupViewport () {
	RenderPath@ renderPath = RenderPath();
	XMLFile@ xml = cache.GetResource("XMLFile","RenderPaths/Forward.xml");
	renderPath.Load(xml);
	Viewport@ viewport = Viewport(gameScene, cameraNodeBis.GetComponent("Camera"), renderPath);
    renderer.viewports[0] = viewport;
}

int x = 0;
float timer = 0;
void HandleUpdate(StringHash eventType, VariantMap& eventData) {
	timer += eventData["TimeStep"].GetFloat();
	if(timer > 0.1f) {
		timer = 0.0f;
		image.SetPixel(x,0,Color(0.0,0.0,0.0,0.0));
		x = (x+1)%7;
		image.SetPixel(x,0,Color(1.0,0.0,0.0,0.0));
		texture.SetData(image,false);
	}
}

// Create XML patch instructions for screen joystick layout specific to this sample app
String patchInstructions =
        "<patch>" +
        "    <add sel=\"/element/element[./attribute[@name='Name' and @value='Hat0']]\">" +
        "        <attribute name=\"Is Visible\" value=\"false\" />" +
        "    </add>" +
        "</patch>";
