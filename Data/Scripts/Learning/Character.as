// Rotator script object class. Script objects to be added to a scene node must implement the empty ScriptObject interface
class Character : ScriptObject
{
    float speed = 5.0f;
    float gravity = 100.0f;

    bool b = false;
    // Update is called during the variable timestep scene update
    void FixedUpdate(float timeStep) {

        UnitMovement@ movementComponent = cast<UnitMovement>(node.GetComponent("UnitMovement"));
        
        Array<Vector3> waypoints(1);
        Vector3 v(50,0,60);
        waypoints[0] = v;

        if(!b) {
            Print("Moving");
            movementComponent.move(waypoints);
            b = true;
        }

        /*RigidBody@ body = node.GetComponent("RigidBody");
        AnimationController@ animCtrl = node.GetComponent("AnimationController");
        
        body.gravityOverride = Vector3(0.0, -gravity, 0.0);

        Quaternion rot = node.rotation;
        Vector3 moveDir(0.0f, 0.0f, 0.0f);
        Vector3 velocity = body.linearVelocity;
        // Velocity on the XZ plane
        Vector3 planeVelocity(velocity.x, 0.0f, velocity.z);

        if (input.keyDown['W'])
            moveDir += Vector3(0.0f, 0.0f, 1.0f);
        if (input.keyDown['S'])
            moveDir += Vector3(0.0f, 0.0f, -1.0f);
        if (input.keyDown['A'])
            moveDir += Vector3(-1.0f, 0.0f, 0.0f);
        if (input.keyDown['D'])
            moveDir += Vector3(1.0f, 0.0f, 0.0f);

        // Normalize move vector so that diagonal strafing is not faster
        if (moveDir.lengthSquared > 0.0f)
            moveDir.Normalize();

        if (moveDir.lengthSquared != 0.0f) {
            animCtrl.PlayExclusive("Models/Jack_Walk.ani", 0, true, 0.2f);
        }
        else {
            animCtrl.Stop("Models/Jack_Walk.ani", 0.2f);
        }

        moveDir = moveDir * speed;

        // If in air, allow control, but slower than when on ground
        body.linearVelocity = moveDir;*/
        
    }

    void Update(float timeStep) {
      
    }
    
}

// Create XML patch instructions for screen joystick layout specific to this sample app
String patchInstructions =
        "<patch>" +
        "    <add sel=\"/element/element[./attribute[@name='Name' and @value='Hat0']]\">" +
        "        <attribute name=\"Is Visible\" value=\"false\" />" +
        "    </add>" +
        "</patch>";
