// Rotator script object class. Script objects to be added to a scene node must implement the empty ScriptObject interface
class TestMapGraphics : ScriptObject
{
    MapGraphics@ graphicsComponent;
    Color paint_color;
    IntVector2 paint_position;
    bool paint = false;
    bool clear_ = false;
    bool reset = false;
    void Start() {

    }
    float timer = 0.0f;
    void Update(float timeStep) {
        timer+=timeStep; if(timer > 0.5f) {

            timer = 0.0f;
            if(scene.GetNode(10).GetComponent("MapData") is null) Print ("SCENE IS NULL"); 
            Component@ dataComponent = null;//(scene.GetNode(10).GetComponent("MapData"));


            /*MapProperties@ propertiesComponent = cast<MapProperties>(scene.GetNode(10).GetComponent("MapProperties"));
                
            MapMouseToTile@ mouseComponent = cast<MapMouseToTile>(scene.GetNode(10).GetComponent("MapMouseToTile"));
            IntVector2 pos = mouseComponent.worldToMap(Vector2( float(input.mousePosition.x) / float(graphics.width) , 
                             float(input.mousePosition.y) / float(graphics.height)));


            Vector3 mapPos = mouseComponent.mapToWorld(IntVector2(6,3));*/

        }

        if(paint) {
            graphicsComponent = cast<MapGraphics>(scene.GetNode(10).GetComponent("MapGraphics"));
            if(graphicsComponent !is null) {
                Print("---------------We'll try to paint...");
                graphicsComponent.paintTile(paint_color, paint_position.x, paint_position.y);
                graphicsComponent.apply();
            }
            else {
                Print("---------------Is null");
            }
            paint = false;
        }
        if(reset) {
            graphicsComponent = cast<MapGraphics>(scene.GetNode(10).GetComponent("MapGraphics"));
            if(graphicsComponent !is null) {
                Print("---------------We'll try to paint...");
                graphicsComponent.resetTile(paint_position.x, paint_position.y);
                graphicsComponent.apply();
            }
            else {
                Print("---------------Is null");
            }
            reset = false;
        }
        if (clear_) {
            graphicsComponent = cast<MapGraphics>(scene.GetNode(10).GetComponent("MapGraphics"));
            if(graphicsComponent !is null) {
                Print("---------------We'll try to clear map...");
                graphicsComponent.clearMap();
                graphicsComponent.apply();
            }
            else {
                Print("---------------Is null");
            }
            clear_ = false;
        }

    }

}

