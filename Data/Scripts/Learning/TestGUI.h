class TestGUI : ScriptObject
{
	Window@ window;

	void Start() {

		// Load XML file containing default UI style sheet
    	XMLFile@ style = cache.GetResource("XMLFile", "UI/DefaultStyle.xml");

    	// Set the loaded style as default style
    	ui.root.defaultStyle = style;

		window = Window();
		ui.root.AddChild(window);

		window.SetMinSize(400,300);

		// Set Window size and layout settings
		window.SetMinSize(384, 192);
		window.SetLayout(LM_VERTICAL, 6, IntRect(6, 6, 6, 6));
		window.SetAlignment(HA_CENTER, VA_CENTER);
		window.name = "Window";

		Text@ name = Text();
		name.name = "Name";
		name.text = "Name: ";

		Text@ hp = Text();
		name.name = "HP";
		name.text = "HP: ";

		Text@ mp = Text();
		name.name = "MP";
		name.text = "MP: ";

		window.AddChild(name);
		window.AddChild(hp);
		window.AddChild(mp);
	}

	void Update() {

	}
}