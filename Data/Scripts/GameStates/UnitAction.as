DmStateInfo@ dmstate; 
DmCommander@ dmcommander;

Node@ selectedUnit;
IntVector2 selectedTile;

Sound@ music;
SoundSource@ source;

void sEnd(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

	commander.GetGUI().SetActionVisible(false);
	UnsubscribeFromEvent("ActionPress");
}

void sInit(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

	commander.GetGUI().SetActionVisible(true);
	SubscribeToEvent("ActionPress", "ButtonPressed");
	dmstate = state;
	dmcommander = commander;

	selectedTile = state.StackAt(0).GetIntVector2();
	selectedUnit = cast<Node@>(state.StackAt(1).GetPtr());
}

void sUpdate(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

}

void ButtonPressed(StringHash eventType, VariantMap& eventData) {
    String s = eventData["Button"].GetString();
    if(s == "AttackButton") {
    	UnitStats@ stats = cast<UnitStats@>(selectedUnit.GetComponent("UnitStats")); 
    	PathfinderInfo@ p = dmcommander.GetPathfinder().GetPathInfo(
    		selectedTile, 0, stats.GetAttackRange(), stats.GetMinAttackRange(),
    		ATTACK, stats.GetTeam());
    	dmcommander.PaintInRange(p, Color(1,0,0), true);
        dmcommander.GetGraphics().PushColors();
        Variant v = dmcommander.GetUnitsInRange(p, true);
        if(v.GetVariantVector().length > 0) {
            dmstate.Push(Variant("MeleeAttack"));//callNext
            dmstate.Push(Variant(-1)); //team
            Variant[] dummy(0);
            dmstate.Push(Variant(dummy));//exclude
            dmstate.Push(Variant(v));//include
            dmstate.stateDone = true;
            dmstate.nextState = "UnitSelect";
        }
    }
    else if(s == "MoveButton") {
    	dmstate.stateDone = true;
    	dmstate.nextState = "PathSelect";
    }
    else if(s == "StayButton") {
        dmstate.stateDone = true;
        dmstate.nextState = "InitTurn";
        dmcommander.GetGraphics().ResetTile(selectedTile.x, selectedTile.y);
    }
}

