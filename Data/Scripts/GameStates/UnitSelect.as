// Waits for the user to select a unit from the map

// @pre: Array<Node@> includedUnits
//		 Array<Node@> excludedUnits
//		 int team // -1 means no filter team
//		 String callNext
// @post: IntVector2 selectedPos
//        Node@ selectedUnit

Variant[] includedUnits;
Variant[] excludedUnits;
Team team;
String callNext;

bool noFilterTeam;
IntVector2 lastTile;
Node@ selectedUnit;

bool okUnit = false;

void sEnd(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

	lastTile = IntVector2(-1,-1);
	selectedUnit = null;
	okUnit = false;
	commander.QueryMouseTile(IntVector2(-2,-2));
    noFilterTeam = false;

}

void sInit(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

	includedUnits = state.Pop().GetVariantVector();
	excludedUnits = state.Pop().GetVariantVector();
    int teamInt = state.Pop().GetInt();
    if(teamInt == -1)
        noFilterTeam = true;
	team = commander.intToTeam(teamInt);
	callNext = state.Pop().GetString();

	commander.GetGUI().SetTileVisible(true);

}

void sUpdate(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

  	IntVector2 tile;
    if(commander.QueryMouseTile(tile) 
      && commander.GetData().TileWalkable(tile.x, tile.y)) {

        commander.GetGraphics().PaintTile(Color(1,0.5,0.25), tile.x, tile.y);
        commander.GetGraphics().ResetTile(lastTile.x, lastTile.y);
        lastTile = tile;

        commander.SetTileGUI(tile);
        selectedUnit = commander.GetData().TileUnit(tile.x, tile.y);
        
        if(selectedUnit != null) {
            commander.GetGUI().SetUnitVisible(true);
            commander.SetUnitGUI(selectedUnit);
            Print("Team "+commander.teamToInt(cast<UnitStats>(selectedUnit.GetComponent("UnitStats")).GetTeam()));
	        okUnit = (included(selectedUnit) and 
                !excluded(selectedUnit) and (noFilterTeam ||
                 cast<UnitStats>(selectedUnit.GetComponent("UnitStats")).GetTeam() == team ));
        }
        else {
            commander.GetGUI().SetUnitVisible(false);
        }

    }

    if(input.mouseButtonPress[MOUSEB_LEFT]) {

    	if(tile.x != -1 and !(selectedUnit is null) and okUnit) {
    		state.Push(Variant(selectedUnit));
            Print(tile.x+" "+tile.y);
    		state.Push(Variant(tile));
    		state.stateDone = true;
    		state.nextState = callNext;
    	}
    }
}

bool included (Node@ unit) {
	if(includedUnits.length == 0) return true;
	for(int i = 0; i < includedUnits.length; ++i) {
		Node@ it = cast<Node@>(includedUnits[i].GetPtr());
		if(it is unit) return true;
	}
	return false;
}

bool excluded (Node@ unit) {
	if(excludedUnits.length == 0) return false;
	for(int i = 0; i < excludedUnits.length; ++i) {
		Node@ it = cast<Node@>(excludedUnits[i].GetPtr());
		if(it is unit) return false;
	}
	return true;

}


