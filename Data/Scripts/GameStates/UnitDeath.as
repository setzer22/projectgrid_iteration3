//Makes the unit die!
//
// @pre:  IntVector2 dyingPos TODO: Remove dyingPos when something decent for death is implemented?
//        Node@ dyingUnit
//        String callNext
//        
// @post: empty stack
//		  	

Node@ dyingUnit;
IntVector2 dyingPos;
UnitStats@ stats; 
AnimationController@ animation;
String callNext;

float timer = 0;

void sEnd(float timeStep, DmStateInfo@ state,
	DmCommander@ commander) {

    timer = 0;
}

void sInit(float timeStep, DmStateInfo@ state,
	DmCommander@ commander) {

    dyingPos = state.Pop().GetIntVector2();
    dyingUnit = cast<Node@>(state.Pop().GetPtr());
    callNext = state.Pop().GetString();
    stats = cast<UnitStats@>(dyingUnit.GetComponent("UnitStats"));
    
    animation.Play("Models/Jack_Walk.ani",1,true,0.0f);
}

void sUpdate(float timeStep, DmStateInfo@ state,
	DmCommander@ commander) {

    timer += timeStep;
    if(timer < 2) {
        commander.GetData().SetUnit(null, dyingPos);
        dyingUnit.GetComponent("AnimatedModel").enabled = false;
        state.stateDone = true;
        state.nextState = callNext;
    }

}
