abstract class GameState {

	void Update(float timeStep, DmStateInfo@ state, 
				DmCommander@ commander) {}

    void Start(float timeStep, DmStateInfo@ state, 
			   DmCommander@ commander) {}

	void End(float timeStep, DmStateInfo@ state, 
			 DmCommander@ commander) {}

    //void UpdateGUI(...) {}
}
