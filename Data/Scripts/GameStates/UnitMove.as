PathfinderInfo@ p;
Node@ selectedUnit;
IntVector2 moveToPos;
UnitMovement@ movement;
MapPathfinder@ pathfinder;
AnimationController@ animation;

void sEnd(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {
}

void sInit(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

  	moveToPos = state.Pop().GetIntVector2();
  	selectedUnit = cast<Node@>(state.Pop().GetPtr());
  	pathfinder = commander.GetPathfinder();
  	p = pathfinder.GetLastPathInfo();
  	IntVector2[]@ path = pathfinder.GetShortestPath(p, moveToPos);
  	for(uint i = 0; i < path.length; ++i) {
  		Print(path[i].x + " " + path[i].y);
  	}
  	movement = cast<UnitMovement@>(selectedUnit.GetComponent("UnitMovement"));
  	MapMouseToTile@ converter = commander.GetMouse();
  	Array<Vector3> waypoints(path.length);	
  	for(int i = 0; i < path.length; ++i) {
  		waypoints[i] = converter.MapToWorld(path[i]);
  	}
  	waypoints.Reverse();
  	movement.Move(waypoints);

    animation = cast<AnimationController@>(selectedUnit.GetComponent("AnimationController"));
    animation.Play("Models/Jack_Walk.ani",1,true,0.0f);


}

void sUpdate(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

    if(movement.GetDone()) {
        commander.GetCamera().SetMode(false);
        commander.GetGraphics().SetVisible(true);
        animation.StopLayer(1,0.1);
            MapData@ data = commander.GetData();
            data.SetUnit(selectedUnit, moveToPos);
            data.SetUnit(null, p.GetSource());
        state.nextState = "InitTurn";
        state.stateDone = true;
        //int id = selectedUnit.vars["SmUnitId"].GetInt();
        //Node@ unit = scene.GetNode(id);
        //unit.position = selectedUnit.position;
        //unit.rotation = selectedUnit.rotation;
    }

}
