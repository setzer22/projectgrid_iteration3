// Waits for the user to select a unit from the map

// @pre: emtpty stack
// @post: IntVector2 selectedPos
//        Node@ selectedUnit


IntVector2 lastTile = IntVector2(-1,-1);
MapGraphics@ gfx;
MapMouseToTile@ mouse;
MapData@ mapData;
Node@ selectedUnit;
CameraMovement@ camera;
Window@ window;

Sound@ music;
SoundSource@ source;

bool hack = false;
Node@ hackUnit;

bool hack2 = false;

Node@ unitsNode; 
Node@ smUnitsNode;
Node@ detailsNode;
Node@ terrainNode;

void sEnd(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {
    lastTile.x = lastTile.y = 0;
    gfx = null;
    mouse = null;
}

void sInit(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

    if(!hack2){ 
      music = cache.GetResource("Sound","Music/field.ogg");
      source = scene.GetNode(1).GetChild("Music").GetComponent("SoundSource");
      source.Play(music);
    }
    hack2 = true;

    gfx = commander.GetGraphics();
    mouse = commander.GetMouse();
    mapData = commander.GetData();
    camera = commander.GetCamera();

    detailsNode = scene.GetChild("Detail");
    unitsNode = scene.GetChild("Units");
    smUnitsNode = scene.GetChild("SmUnits");
    terrainNode = scene.GetChild("Terrain");


    commander.GetGUI().SetTileVisible(true);
    commander.GetGUI().SetActionVisible(true);

    commander.GetDetail().SetHighDetail(false);
    SubscribeToEvent("ActionPress", "ButtonPressed");
}

void ButtonPressed(StringHash eventType, VariantMap& eventData) {
    String s = eventData["Button"].GetString();
    Print(s+" has been pressed");
    if(s == "AttackButton") {
    }
    else if(s == "MoveButton") {
    }
}

void sUpdate(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

    IntVector2 tile;
    if(commander.QueryMouseTile(tile) 
      && mapData.TileWalkable(tile.x, tile.y)) {

        gfx.PaintTile(Color(1,0,0), tile.x, tile.y);
        gfx.ResetTile(lastTile.x, lastTile.y);
        lastTile.x = tile.x; lastTile.y = tile.y;
        if(!hack) {
            commander.SetTileGUI(tile);
            selectedUnit = mapData.TileUnit(tile.x, tile.y);
            
            if(selectedUnit != null) {
                commander.GetGUI().SetUnitVisible(true);
                commander.SetUnitGUI(selectedUnit);
            }
            else {
                commander.GetGUI().SetUnitVisible(false);
            }
        }
    }
    if(input.mouseButtonPress[MOUSEB_LEFT]) {
      if(!(selectedUnit is null)) {
        Variant[] v;
        state.Push(Variant("PathSelect"));
        state.Push(Variant(commander.teamToInt(ALLY)));
        state.Push(Variant(v));
        state.Push(Variant(v));
        state.nextState = "UnitSelect2";
        state.stateDone = true;
      }
    }
    if(input.mouseButtonPress[MOUSEB_RIGHT]) {
      if(!(selectedUnit is null) && !hack) {

        commander.GetDetail().SetHighDetail(true);

        Sound@ sound2 = cache.GetResource("Sound","Music/attack.ogg");
        source.Play(sound2);
        hack = true;
        hackUnit = selectedUnit;
        int id = selectedUnit.vars["SmUnitId"].GetInt();
        Node@ unit = scene.GetNode(id);
        camera.SetUnitNode(unit);
        camera.SetMode(true);
        gfx.SetVisible(false);
      }
      else if(hack) {

        commander.GetDetail().SetHighDetail(false);

        source.Play(music);
        hack = false;
        camera.SetMode(false);
        gfx.SetVisible(true);
        hackUnit.SetScale(5);
      }
    }

    
}
