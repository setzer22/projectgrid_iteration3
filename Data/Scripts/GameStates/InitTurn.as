void sEnd(float timeStep, DmStateInfo@ state,
	DmCommander@ commander) {

}

void sInit(float timeStep, DmStateInfo@ state,
	DmCommander@ commander) {

	//Swap the current team
	if(state.currentTeam == ENEMY) state.currentTeam = ALLY; 
	else state.currentTeam = ENEMY;

	//Initialise Units
	int count = state.TeamSize(state.currentTeam);
	for(int i = 0; i < count; ++i) {
		UnitStats@ stats = state.GetUnit(i,state.currentTeam).
		  GetComponent("UnitStats");
		stats.SetActionPoints(2);
	}

	state.stateDone = true;
	state.nextState = "PlayerTurn";

}

void sUpdate(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {
    
}
