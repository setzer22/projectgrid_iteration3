Sound@ music;
SoundSource@ source;

void sEnd(float timeStep, DmStateInfo@ state,
	DmCommander@ commander) {

}

void sInit(float timeStep, DmStateInfo@ state,
	DmCommander@ commander) {

	//Music
	music = cache.GetResource("Sound","Music/field.ogg");
    source = scene.GetChild("Music").GetComponent("SoundSource");
    source.Play(music);

    //Menu initialization
    commander.GetGUI().SetUnitVisible(false);
    commander.GetGUI().SetTileVisible(false);
    commander.GetGUI().SetActionVisible(false);

    //Detail level is low at start
    commander.GetDetail().SetHighDetail(false);

    //Workaround - Starting team is ally,
    //but we know InitTurn will swap it
    state.currentTeam = ENEMY;

    state.nextState = "InitTurn";
    state.stateDone = true;
  
}

void sUpdate(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {
    
}
