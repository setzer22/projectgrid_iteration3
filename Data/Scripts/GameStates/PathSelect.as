// Calculates shortest paths for the selected unit and displays
// the path from the unit to the mouse position in the map

// @pre: IntVector2 selectedPos
//        Node@ selectedUnit
// @post: IntVector2 moveToPos 
//		  Node@ selectedUnit
//		  	

MapPathfinder@ pathfinder;
MapGraphics@ gfx;
PathfinderInfo@ p;
IntVector2 mouseTile;
Node@ unit;
UnitStats@ unitStats;
IntVector2 unitTile;
Array<IntVector2>@ path;

void sEnd(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {
	gfx.ClearMap();
	commander.QueryMouseTile(IntVector2(-2,-2));
}

void sInit(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {
	unitTile = state.Pop().GetIntVector2();
	unit = cast<Node@>(state.Pop().GetPtr());
	pathfinder = commander.GetPathfinder();
	unitStats = unit.GetComponent("UnitStats");
	p = pathfinder.GetPathInfo(
		unitTile, unitStats.GetMoveRange(),
		unitStats.GetAttackRange(), unitStats.GetMinAttackRange(),
		WALK, ALLY);

	gfx = commander.GetGraphics();
	commander.PaintInRange(p,Color(0,1,0));
}

IntVector2 lastTile = IntVector2(-1,-1);
void sUpdate(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

  	if(commander.QueryMouseTile(mouseTile)) {
  		gfx.ResetTile(lastTile.x, lastTile.y);
  		lastTile = mouseTile;
  		gfx.PaintTile(Color(1,0,0), mouseTile.x, mouseTile.y);
	  	if(mouseTile.x != -1) {
			@path = pathfinder.GetShortestPath(p,mouseTile);
			commander.PaintInRange(p,Color(0,1,0));
			if(path[0].x != -1)
			gfx.PaintTiles(Color(0,0,1), path);
		}
	}	
	if(input.mouseButtonPress[1]) {
		IntVector2 tile = commander.GetMouseTile();
		if(tile.x != -1 && commander.GetData().TileWalkable(mouseTile) && tile != unitTile) {
			state.Push(Variant(unit));
			state.Push(Variant(tile));
			state.nextState = "UnitMove";
			state.stateDone = true;
		}
	}
}

