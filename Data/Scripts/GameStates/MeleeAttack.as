// Performs a melee attack scene and then ends the action 

// @pre: IntVector2 attackedPos
//        Node@ attackedUnit
//		  IntVector2 attackingPos
//        Node@ attackingUnit
//		 
// @post: empty stack

Node@ attackingUnit, attackedUnit;
IntVector2 attackedPos, attackingPos;

float timer;

void sEnd(float timeStep, DmStateInfo@ state,
	DmCommander@ commander) {

	Sound@ music = cache.GetResource("Sound","Music/field.ogg");
	SoundSource@ source = scene.GetChild("Music").GetComponent("SoundSource");
	source.Play(music);

	commander.GetDetail().SetHighDetail(false);
	commander.GetCamera().SetMode(false);
	commander.GetGraphics().SetVisible(true);
}

void sInit(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

	Sound@ music = cache.GetResource("Sound","Music/attack.ogg");
	SoundSource@ source = scene.GetChild("Music").GetComponent("SoundSource");
	source.Play(music);

	attackedPos = state.Pop().GetIntVector2();
	attackedUnit = cast<Node@>(state.Pop().GetPtr());
	attackingPos = state.Pop().GetIntVector2();
	attackingUnit = cast<Node@>(state.Pop().GetPtr());

	Node@ smUnit = scene.GetNode(attackingUnit.vars["SmUnitId"].GetInt());
    Node@ smUnit2 = scene.GetNode(attackedUnit.vars["SmUnitId"].GetInt());

    commander.GetCamera().SetMeleeCombat(smUnit, smUnit2);
	commander.GetCamera().SetMode(true);
	commander.GetGraphics().SetVisible(false);
	commander.GetGraphics().ClearMap();
	commander.GetDetail().SetHighDetail(true);

	timer = 0;
}

void sUpdate(float timeStep, DmStateInfo@ state,
  DmCommander@ commander) {

	timer += timeStep;
	if(timer > 5.0f) {
        UnitStats@ stats = attackedUnit.GetComponent("UnitStats");
        stats.SetHP(stats.GetHP()-50);
        state.stateDone = true;
        if(stats.GetHP() <= 0) {
            state.Push(Variant("InitTurn"));
            state.Push(Variant(attackedUnit));
            state.Push(Variant(attackedPos));
            state.nextState = "UnitDeath";
        }
        else {
            state.nextState = "InitTurn";
        }
	  commander.GetGraphics().PopColors();
	}
}
