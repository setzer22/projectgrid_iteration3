const String GRID_COLORMUL = "grid_ColorMultiplier";
const String GRID_DIM = "grid_Dimensions";
const String GRID_E = "grid_GridWidth";
const String GRID_ORIGIN = "grid_Origin";
const String GRID_ROUND = "grid_RoundCorner";
const String GRID_S = "grid_SquareSize";
const String GRID_TEXSIZE = "grid_TexSize";


// Rotator script object class. Script objects to be added to a scene node must implement the empty ScriptObject interface
class MapGraphics_SCRIPT : ScriptObject {
	
	IntVector2 dimensions = IntVector2(7,8);
	Vector3 origin;
	int terrainNodeId = 9;

	Texture2D@ terrainTexture;
	Material@ terrainMaterial;
	Image@ originalData;
	
	private	String mapFilename;
	private Image@ currentData;
	
	void Start () {

		MapGraphics@ graphics = cast<MapGraphics&>(scene.GetNode(10).GetComponent("MapGraphics"));


		//Link with the properties here
		//properties.getDimensions()...
		
		int mapW = dimensions.x;
		int mapH = dimensions.y;
		
		originalData = cache.GetResource("Image", "Textures/TileMap1.png");
		currentData = Image();
		currentData.SetSize(originalData.width, originalData.height, 4);
		for(int i = 0; i < originalData.width; ++i) {
		for(int j = 0; j < originalData.height; ++j) {
				currentData.SetPixel(i,j,originalData.GetPixel(i,j));
			}
		}

		terrainMaterial = cache.GetResource("Material", "Materials/GridedTerrain.xml");
		terrainTexture = terrainMaterial.textures[4];
		terrainTexture.SetData(currentData);

		//The true work of a hardcoder
		terrainMaterial.shaderParameters[GRID_S] = 10.0f;
		terrainMaterial.shaderParameters[GRID_DIM] = Variant(Vector2(dimensions.x, dimensions.y));
		terrainMaterial.shaderParameters[GRID_E] = 0.5f;
		terrainMaterial.shaderParameters[GRID_ORIGIN] = Variant(Vector2(0, 0));
		terrainMaterial.shaderParameters[GRID_COLORMUL] = 0.5f;
		terrainMaterial.shaderParameters[GRID_ROUND] = 1.1f;
		terrainMaterial.shaderParameters[GRID_TEXSIZE] = Variant(Vector2(8, 8));

		cast<Terrain>(scene.GetNode(terrainNodeId).GetComponent("Terrain")).material = terrainMaterial;

	}
	
    void Update(float timeStep) {

    }

	void paintTiles (Color color, IntVector2[] positions) {
		for(int i = 0; i < positions.length; ++i) {
			paintTile(color, positions[i].x, positions[i].y);
		}
	}    

	void paintTile (Color color, int x, int y) {
		//TODO: Check if this paints the correct tile
		currentData.SetPixel(x, dimensions.y - y - 1, color);
	}

	void resetTile (int x, int y) {
		currentData.SetPixel (x, y, originalData.GetPixel (x, y));
	}

	void clearMap () {

		for (int i = 0; i < dimensions.x; ++i) {
			for (int j = 0; j < dimensions.y; ++j) {
				resetTile(i,j);
			}
		}
	}

	void apply () {
		//kept so code won't break while porting
	}


}

// Create XML patch instructions for screen joystick layout specific to this sample app
String patchInstructions =
        "<patch>" +
        "    <add sel=\"/element/element[./attribute[@name='Name' and @value='Hat0']]\">" +
        "        <attribute name=\"Is Visible\" value=\"false\" />" +
        "    </add>" +
        "</patch>";
